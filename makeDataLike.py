from prot.utiltools import *
from prot.plottools import *
from prot.filetools import *
from prot.canvastools import *
from prot import batchtools

import ROOT

import dataliketools
import runtools

import subprocess
import shutil
import glob
import os.path

def plotDataLike(effEntHist,scaledHist,dataLikeHist,**kwargs):
    # Plot
    plots([(scaledHist,{'opt':'hist'}),
           (effEntHist,{'opt':'hist','color':ROOT.kBlue}),
           (dataLikeHist,{'color':ROOT.kRed})],
           ytitle='Events',
           logy=True,xrange=(169,1493),yrange=(1e-1,None),hsopt='nostack',legend=(0.2,0.4),**kwargs)

def runDataLike(inpath,outpath,outLumis=[0.01,0.1,0.5,1.,3.,5.,7.,9.,10.,20.],ignoreNeff=False,histname='Zprime_mjj',nameTag='',**kwargs):
    #
    # Input
    fh_in=filetools.open(inpath)

    stitle=None
    if os.path.basename(inpath)[0:5]=='hist-': # split
        stitle=os.path.basename(inpath)[5:-5]
    else: # non-split
        stitle=os.path.basename(inpath)[:-5]

    hist=fh_in.Get(histname)

    selection=os.path.dirname(histname)
    histname=os.path.basename(histname)

    #
    # Output
    fh_out=filetools.open(outpath,'RECREATE')

    # Make data like
    effEntHist,scaledHists,dataLikeHists=dataliketools.makeDataLikeFromHist(hist,outLumi=outLumis,ignoreNeff=ignoreNeff,nameTag=nameTag)

    # Save
    if(selection!=''): fh_out.mkdir(selection).cd()
    effEntHist.Write()
    for i in range(len(outLumis)):
        scaledHist=scaledHists[i]
        dataLikeHist=dataLikeHists[i]
        outLumi=outLumis[i]

        plotDataLike(effEntHist,scaledHist,dataLikeHist,**kwargs)
        style.ATLAS(selection,xlabel=0.52,ylabel=0.25,lumi=outLumi)
        style.textbox(stitle,xlabel=0.01,ylabel=1.02,size=0.03)
        c1=canvastools.canvas()
        c1.Write('Neff-%s-%s-%s'%(selection,stitle,outLumi))

        scaledHist.Write()
        dataLikeHist.Write()

    fh_out.Close()

def main(inPath,lumiData='data/data_HLT_j380.csv',outName='datalike',outLumis=[0.5,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,12.,14.,16.,18.,20.],ignoreNeff=False,split=None,histname='Zprime_mjj_var',nameTag='',**kwargs):    
    outLumis=runtools.RunList(lumiData).lumisteps() if outLumis==None else outLumis

    inpaths=glob.glob(inPath)
    bkgdir=os.path.dirname(inPath)

    outdir=bkgdir+'/'+outName
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    outfiles=[]

    job=batchtools.get_batch('makeDataLike')
    outfiles=[]
    for inpath in inpaths:
        job.data.add(inpath)

        stitle=os.path.basename(inpath)[:-5]
        outpath=stitle+'.root'
        outfiles.append(outpath)        
        job.output.add(outpath)

        # Loop over selections
        cmdkwargs={'outLumis':outLumis,'ignoreNeff':ignoreNeff,'nameTag':nameTag}
        cmdkwargs.update(kwargs)
        if histname=='Zprime_mjj_var': cmdkwargs['dijetard']=True        

        fh_in=filetools.open(inpath)
        for key in fh_in.GetListOfKeys():
            obj=key.ReadObj()
            if not obj.InheritsFrom(ROOT.TDirectoryFile.Class()): continue
            selection=key.GetName()
            #if selection not in ['dijetgamma_g150_2j25']: continue
            cmdkwargs['histname']='%s/%s'%(selection,histname)

            job.commands.append((runDataLike,[inpath,outpath],cmdkwargs.copy()))

    datadir=job.run()

    # Move outputs around
    srcPaths=['%s/%s'%(datadir,outfile) for outfile in outfiles]
    for srcPath in  srcPaths:
        dstPath=outdir+'/'+os.path.basename(srcPath)
        if os.path.exists(dstPath): os.unlink(dstPath)
        shutil.move(srcPath,dstPath)

    # Merge split files
    if split:
        splitPath='%s/%s'%(outdir,split)
        if os.path.exists(splitPath): os.unlink(splitPath)
        subprocess.call(['hadd',splitPath]+['%s/%s'%(outdir,outfile) for outfile in outfiles])
        outfiles.append(split)

