import ROOT
import re

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import style

import zprimetools
import selectiontools

from math import *

def dosoversqrtb(h_bkg,h_sig,mjjrange,cutdir='<'):
    s=0
    b=0
    h_soversqrtb=h_bkg.Clone()
    h_soversqrtb.Reset()
    h_soversqrtb.GetYaxis().SetTitle('Cumulative s/#sqrt{b}')

    cutrange=range(h_soversqrtb.GetNbinsX()+2) if cutdir=='<' else range(h_soversqrtb.GetNbinsX()+1,-1,-1)
    for binIdx in cutrange:
        s+=h_sig.GetBinContent(binIdx)
        b+=h_bkg.GetBinContent(binIdx)
        if s==0 and b==0: continue
        if b==0: continue
        if b<1e-2: continue
        h_soversqrtb.SetBinContent(binIdx,s/sqrt(b))
    
    return h_soversqrtb;

def runAllCutStudy(d_bkg,d_sigs,**kwargs):
    lumi=kwargs.get('lumi',1)
    d_bkg=utiltools.filetag(d_bkg)
    
    # get list of available background distributions
    mjjslices={}
    re_mjjslice=re.compile('Mjj([0-9]+)to([0-9]+)')
    for key in d_bkg[0].GetListOfKeys():
        if not key.ReadObj().InheritsFrom(ROOT.TDirectoryFile.Class()): continue
        match=re_mjjslice.match(key.GetName())
        if match==None: continue
        mjjslices[(int(match.group(1)),int(match.group(2)))]=key.GetName()

    #
    # Loop over signals
    h_soversqrtbs={}
    for d_sig in d_sigs:
        d_sig=utiltools.filetag(d_sig)
        if d_sig[0]==None: continue

        # Figure out best mass range for optimization study
        nameTag=zprimetools.nameTag(d_sig[0].GetFile())
        canvastools.saveextra=nameTag

        Zprime_mjj=d_sig[0].Get('Zprime_mjj')
        peakmjj=Zprime_mjj.GetBinCenter(Zprime_mjj.GetMaximumBin())
        d_bkgslice=None
        d_sigslice=None
        mjjrange=None
        edgeclose=None
        for key,value in mjjslices.items():
            if key[0]<peakmjj<key[1]:
                edge=min(abs(key[0]-peakmjj),abs(key[1]-peakmjj))
                print(key,peakmjj,edge)
                if edgeclose!=None and edge<edgeclose: continue
                d_bkgslice=d_bkg[0].Get(value)
                d_sigslice=d_sig[0].Get(value)
                mjjrange=key
                edgeclose=edge

        #
        # calculate s/sqrt(b)
        rankings=[]

        # The loop over variables
        for key in d_bkg[0].GetListOfKeys():
            if not key.ReadObj().InheritsFrom(ROOT.TH1F.Class()): continue
            if 'cutflow' in key.GetName(): continue

            varname=key.GetName()
            #if varname!='ZprimeISR_dPhiISRclosej': continue
            if varname not in h_soversqrtbs: h_soversqrtbs[varname]=[]

            h_bkg=d_bkgslice.Get(key.GetName())
            h_sig=d_sigslice.Get(key.GetName())

            if h_sig==None: continue # missing histogram for this guy
            #if h_sig.GetName()!='ZprimeISR_asymISRjj': continue

            # Calculate s/sqrtb in both directions
            h_soversqrtblt=dosoversqrtb(h_bkg,h_sig,mjjrange=mjjrange,cutdir='<')
            maxsoversqrtblt=h_soversqrtblt.GetMaximum()

            h_soversqrtbgt=dosoversqrtb(h_bkg,h_sig,mjjrange=mjjrange,cutdir='>')
            maxsoversqrtbgt=h_soversqrtbgt.GetMaximum()

            # Pick the more optimal direction
            h_soversqrtb=None
            maxsoversqrtb=None
            if maxsoversqrtblt>maxsoversqrtbgt:
                h_soversqrtb=h_soversqrtblt
                maxsoversqrtb=maxsoversqrtblt
            else:
                h_soversqrtb=h_soversqrtbgt
                maxsoversqrtb=maxsoversqrtbgt

            maxbin=h_soversqrtb.GetMaximumBin()
            cut=h_soversqrtb.GetBinCenter(maxbin)

            # plot
            plottools.plots([(h_bkg,{'title':'bkg'}),
                             (h_sig,{'title':'sig',
                                     'linecolor':ROOT.kBlue}),
                             (h_soversqrtb,{'title':'s/#sqrt{b}',
                                            'linestyle':ROOT.kDashed,
                                            'linecolor':ROOT.kBlue})],
                            opt='hist',hsopt='nostack',ytitle='a.u.',nounits=True,normalize=True,
                            text='%g GeV < m_{jj} < %g GeV\nOptimal cut: %g'%(mjjrange[0],mjjrange[1],cut),textpos=(0.55,0.73),
                            legend=(0.8,0.6))
            canvastools.save(h_soversqrtb.GetName()+'.pdf')
            rankings.append((varname,maxsoversqrtb))

            h_soversqrtbs[varname].append((h_soversqrtb,d_sig[1]))

        #
        # save rankings
        rankings=sorted(rankings,key=lambda x: x[1])

        # Add no extra cuts s/sqrt(b)
        bkg_mjj=d_bkgslice.Get('Zprime_mjj')        
        sig_mjj=d_sigslice.Get('Zprime_mjj')
        rankings.append(('nocuts',sig_mjj.Integral()/sqrt(bkg_mjj.Integral())))

        fh_r  =open(canvastools.savefullpath(True)+'/rankings.txt','w')
        fh_tex=open(canvastools.savefullpath(True)+'/rankings.tex','w')
        texprefix=kwargs.get('texprefix','')+nameTag+'/'+filetools.objectpath(d_bkg[0])
        for varname,maxsoversqrtb in rankings:
            significance=maxsoversqrtb*sqrt(lumi)

            texvarname=textools.sanitizestring('%s%s'%(texprefix,varname))

            fh_r  .write('%s\t%0.2f\n'%(varname   ,significance))
            fh_tex.write('\\newcommand{\\%s}{%0.2f}\n'%(texvarname,significance))
        fh_r.close()
        fh_tex.close()
            
    # Plot comparison
    canvastools.saveextra=''
    for varname,h_soversqrtb in h_soversqrtbs.items():
        #if varname!='ZprimeISR_asymISRjj': continue
        plottools.plots(h_soversqrtb,hsopt='nostack',legend=(0.6,0.9),normalize=True,ratio=None,default=None,opt='HIST',text=d_bkg[0].GetName(),textpos=(0.5,0.25),logy=True,ytitle='a.u.')

        # Draw help lines
        for hist,opt in h_soversqrtb:
            if hist.Integral()==0: continue
            maxbin=hist.GetMaximumBin()
            cut=hist.GetBinCenter(maxbin)
            val=hist.GetBinContent(maxbin)/hist.Integral()

            l=ROOT.TLine()
            l.SetLineStyle(ROOT.kDashed)
            style.style.apply_style(l,opt)
            l.DrawLine(cut,0,cut,val)
        
        canvastools.save('%s.pdf'%varname)
        
def main(filebkg,filesigs,**kwargs):
    canvastools.savesetup('soversqrtb',**kwargs)

    selectiontools.loop(runAllCutStudy,filebkg,filesigs,**kwargs)

    canvastools.savereset()
