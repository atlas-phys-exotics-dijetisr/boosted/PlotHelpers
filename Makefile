CPPFLAGS=`root-config --cflags` -fPIC -g
LDFLAGS=`root-config --ldflags --libs` -g

DEPS=
OBJS=treePlot.o Style.o Canvas.o File.o Fit.o Plot.o Plot2D.o PlotGraph.o Utils.o

%.o: %.cpp %.h
	g++ -c -o $@ $< $(CPPFLAGS)

all: ${OBJS}
	g++ ${OBJS} atlasstyle/AtlasLabels.C atlasstyle/AtlasUtils.C -shared -o ana.so ${LDFLAGS} ${CPPFLAGS}

clean:
	rm ${OBJS}
