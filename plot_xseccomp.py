import ROOT

from prot import plottools
from prot import utiltools
from prot import treetools
from prot import filetools
from prot import canvastools
from prot import style

def main(xsectrees):
    gSMs=[0.3] #[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
    mRs=[100,200,300,400,500,600,700,800,900]

    #
    # vs mR
    for gSM in gSMs:
        graphs=[]
        for xsectree in xsectrees:
            print(xsectree)
            xsectree=utiltools.filetag(xsectree)
            graph=treetools.treeGraph(xsectree[0],
                                      "mR",
                                      "xsec*filteff",
                                      "gSM==%0.1f"%gSM,
                                      xtitle="m_{R} [GeV]",ytitle="[pb]",
                                      **xsectree[1])
            graphs.append(graph)

        plottools.graphs(graphs,
                         mgopt='AC',
                         yrange=(1e-2,1e5),xrange=(200,1000),
                         ytitle='Signal Cross-Section [pb]',
                         ratio=0,ratiorange=(5e-5,2e-2),
                         logy=True,
                         text='g_{SM}=%0.2f'%(gSM),textpos=(0.55,0.8),
                         legend=(0.2,0.65),
                         linewidth=2)
        canvastools.save("info_xsecvsmR_gSM%0.2f.pdf"%gSM)
