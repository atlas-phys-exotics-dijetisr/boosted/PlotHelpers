#!/bin/bash

#
# Get python versions
OLDTERM=${TERM}
export TERM=""

IPYVER=0
if [ "x$(command -v ipython)x" != "xx" ]; then
    IPYVER=$(ipython -c "import sys; print('%d%d'%sys.version_info[0:2])")
fi

PYVER=0
if [ "x$(command -v python)x" != "xx" ]; then
    PYVER=$(python -c "import sys; print('%d%d'%sys.version_info[0:2])")
fi

export TERM=${OLDTERM}

#
# Execute correct python
echo ${IPYVER}
if [[ "${IPYVER//[!0-9]}" -ge 27 ]]; then
    #echo "ipython"
    ipython -i ${WORKDIR}/prot.py -- ${@}
elif [[ "${PYVER//[!0-9]}" -ge 27 ]]; then
    #echo "python"
    python -i ${WORKDIR}/prot.py -- ${@}
else 
    echo "Need python 2.7 or better!"
fi