#
# Run gaussian limit setting code

from prot import filetools
from prot import fittools
from prot import canvastools
from prot import batchtools
from prot import plottools

import ROOT

import selectiontools

import subprocess
import shutil
import glob
import os.path

def runGaussian(infile,inhist,width):
    #
    # Generate configuration file
    fh=open('limit.config','w')
    fh.write('inputFileName     %s\n'%infile)
    fh.write('dataHist          %s\n'%inhist)
    fh.write('outputFileName    tmplimits\n')
    fh.write('Ecm              13000.0\n')
    fh.write('useBW		        false\n')
    fh.write('startPoints        570\n')
    fh.write('stopDensePoints   1200\n')
    fh.write('stopMediumPoints  1200\n')
    fh.write('stopPoints        1200\n')
    fh.write('minXForFit          -1\n')
    fh.write('maxXForFit          -1\n')
    fh.write('functionCode		   9\n')
    fh.write('nParameters 		   3\n')
    fh.write('parameter1            1033\n')
    fh.write('parameter2            30  \n')
    fh.write('parameter3            -1.9\n')
    fh.write('doAlternateFunction false\n')
    fh.write('nSigmas              3.\n')
    fh.write('doFitError           true\n')
    fh.write('nFitsInBkgError    100\n')
    fh.write('doLumiError          true\n')
    fh.write('luminosityErr        0.09\n')
    fh.write('doJES               false\n')
    fh.write('doVarJES	          false\n')
    fh.write('nJES                 25\n')
    fh.write('sigmaJESShift	       0.10\n')
    fh.write('doFitFunctionChoiceError false\n')
    fh.close()

    # Run 
    cmd=['doGaussianLimits','--ratio',str(width),'--config','limit.config']
    subprocess.call(cmd)

    # Copy output
    selection=inhist.split('/')[0]
    fh_in=filetools.open('tmplimits_%d.root'%(width*1000))
    fh_out=filetools.open('limits570to1200_%d.root'%(width*1000),'recreate')
    fh_out.mkdir(selection).cd()
    for key in fh_in.GetListOfKeys():
        obj=key.ReadObj()
        obj.Write(key.GetName())
    fh_out.Close()

def main(infile,widths=[0.03,0.05,0.1]):
    ### Get input data file ###
    fileIn = filetools.open(infile)
    if not fileIn:
        print("ERROR, could not find file ", infile)
        exit(1)

    # Process selections
    job=batchtools.get_batch('runGaussian')
    job.data.add('RootCore.par')
    job.data.add(infile)

    for key in fileIn.GetListOfKeys():
        selection=key.GetName()
        if selection not in ['dibjet60_blinded','dibjet70_blinded','dibjet77_blinded','dibjet85_blinded']: continue
        #if selection!='dibjet70': continue
        for width in widths:
            job.output.add('limits570to1200_%d.root'%(width*1000))
            job.commands.append((runGaussian,[infile,
                                              selection+'/data',
                                              width]))

    fileIn.Close()
    tempdir=job.run()

    for out in job.output:
        shutil.copyfile(tempdir+'/'+out,os.path.dirname(infile)+'/'+out)
