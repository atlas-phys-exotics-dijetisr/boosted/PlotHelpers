import array
import glob
import re
import os, os.path

from prot import filetools

import ROOT

class xsectree:
    def __init__(self):
        self.tree=ROOT.TTree('tree','tree')

        self.mR     =array.array('d',[0.])
        self.mDM    =array.array('d',[0.])
        self.gSM    =array.array('d',[0.])
        self.gDM    =array.array('d',[0.])
        self.width  =array.array('d',[0.])
        self.xsec   =array.array('d',[0.])
        self.nEvents=array.array('d',[0.])
        self.filteff=array.array('d',[0.])
        self.seleff =array.array('d',[0.])
        self.acc    =array.array('d',[0.])
        self.eff    =array.array('d',[0.])

        self.tree.Branch('mR'     ,self.mR     ,'mR/D')
        self.tree.Branch('mDM'    ,self.mDM    ,'mDM/D')
        self.tree.Branch('gSM'    ,self.gSM    ,'gSM/D')
        self.tree.Branch('gDM'    ,self.gDM    ,'gDM/D')
        self.tree.Branch('width'  ,self.width  ,'width/D')
        self.tree.Branch('xsec'   ,self.xsec   ,'xsec/D')
        self.tree.Branch('nEvents',self.nEvents,'nEvents/D')
        self.tree.Branch('filteff',self.filteff,'filteff/D')
        self.tree.Branch('seleff' ,self.seleff ,'seleff/D')
        self.tree.Branch('acc'    ,self.acc    ,'acc/D')
        self.tree.Branch('eff'    ,self.eff    ,'eff/D')

        self.width[0]  =0.
        self.nEvents[0]=0.
        self.filteff[0]=1.
        self.seleff[0] =1.
        self.acc[0]    =1.
        self.eff[0]    =1.

    def fill(self,mR,mDM,gSM,gDM,d_sel,d_truth=None):
        self.mR [0]=mR
        self.mDM[0]=mDM
        self.gSM[0]=gSM
        self.gDM[0]=gDM
        
        h=d_sel.Get('Zprime_mjj')
        self.xsec[0]=h.Integral()

        cutflow=d_sel.Get('cutflow')
        for binIdx in range(1,cutflow.GetNbinsX()+1):
            if cutflow.GetXaxis().GetBinLabel(binIdx)=='': break
            self.seleff[0]=cutflow.GetBinContent(binIdx)

        self.acc[0]=self.seleff[0]
        self.eff[0]=1.

        if d_truth!=None:
            cutflow=d_truth.Get('cutflow')
            for binIdx in range(1,cutflow.GetNbinsX()+1):
                if cutflow.GetXaxis().GetBinLabel(binIdx)=='': break
                self.acc[0]=cutflow.GetBinContent(binIdx)
            self.eff[0]=self.seleff[0]/self.acc[0]

        self.tree.Fill()

def main(infilter,outpath,truthpath=None):
    # Prepare out tree
    fh_out=filetools.open(outpath,'RECREATE')

    trees={}

    # Fill
    re_histname_truth=re.compile('.*mR([0-9]+)_mDM([0-9]+)_gSM([0-9]+p[0-9]+)_gDM([0-9]+p[0-9]+).*')
    re_histname=re.compile('.*mR([0-9]*p[0-9]+)_mD([0-9]+)_gS([0-9]*p[0-9]+)_gD([0-9]+).*')
    histpaths=glob.glob(infilter)

    for histpath in histpaths:
        match_truth=re_histname_truth.match(os.path.basename(histpath))
        match=match_truth
        if match_truth==None:
            match=re_histname.match(os.path.basename(histpath))
            if match==None: continue
        mR =int(match.group(1)) if match_truth!=None else int(float(match.group(1).replace('p','.'))*1000)
        mDM=int(match.group(2)) if match_truth!=None else int(match.group(2).replace('p','.'))*1000
        gSM=float(match.group(3).replace('p','.'))
        gDM=float(match.group(4).replace('p','.'))

        fh=filetools.open(histpath)
        fh_truth=None
        if truthpath!=None:
            mcname='.'.join(os.path.basename(histpath).split('.')[2:5])
            files=glob.glob('%s/*%s*'%(truthpath,mcname))
            if len(files)>0:
                fh_truth=filetools.open(files[0])

        for key in fh.GetListOfKeys():
            if not key.ReadObj().InheritsFrom(ROOT.TDirectoryFile.Class()): continue
            selection=key.GetName()
            if selection not in trees:
                tree=xsectree()
                trees[selection]=tree
            tree=trees[selection]
            tree.fill(mR,mDM,gSM,gDM,key.ReadObj(),fh_truth.Get(selection) if fh_truth!=None else None)

    # save
    fh_out.cd()
    for selection,tree in trees.items():
        fh_out.mkdir(selection).cd()
        tree.tree.Write()
    fh_out.Close()
