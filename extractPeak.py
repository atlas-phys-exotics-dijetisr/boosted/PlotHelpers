import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import fittools
from prot import style

import zprimetools
import dijettools
import selectiontools

import glob
import math
import shutil
import copy
import datetime
import os, os.path

func=ROOT.TF1("peakbkg","gaus+dijet3par",0,1000)
#func=ROOT.TF1("peakbkg","gaus+landau",0,1000)
#func=ROOT.TF1("peakbkg","gaus",0,1000)
#func=ROOT.TF1("peakbkg","dijet3par",0,1000)

def extractPeak(d_sig,**kwargs):
    mR,mDM,gSM,gDM=zprimetools.info(os.path.basename(d_sig[0].GetFile().GetPath()[:-2]))
    
    mjj=d_sig[0].Get('Zprime_mjj')
    peakpos=mjj.GetMaximumBin()
    peakmjj=mjj.GetBinCenter(peakpos)

    func.SetParameter(0, 0.03)
    func.SetParameter(1, peakmjj)
    func.SetParameter(2, 50)
    result=fittools.fit(mjj,'peakbkg',fitrange=(max(mR-250,200),1000),text='m_{R} = %d GeV, g_{SM}=%0.1f'%(mR,gSM),textpos=(0.6,0.7))

    A     =result.Parameter(0)
    mean  =result.Parameter(1)
    stddev=result.Parameter(2)

    total=mjj.Integral()
    peak=A*math.sqrt(2*math.pi)*abs(stddev)/10
    acc=peak/total

    minx=mjj.FindBin(mean-2*stddev)
    maxx=mjj.FindBin(mean+2*stddev)
    minx=mjj.FindBin(300)
    maxx=mjj.FindBin(610)
    total=mjj.Integral(minx,maxx)
    #peak*=0.95
    acc_core=peak/total

    print('WIDTH IS',stddev/mean)

    canvastools.save('peakFit-mR%d.svg'%mR)

    print(mR,stddev/mean)

    return acc,acc_core

def extractPeaks(*d_sigs,**kwargs):
    glimits={}
    glimits[400]= 0.20105
    glimits[450]= 0.34636
    glimits[500]= 0.16499

    g_gaussian=ROOT.TGraph()
    gidx=0
    for mR,glim in glimits.items():
        g_gaussian.SetPoint(gidx, mR, glim)
        gidx+=1

    zlimits={}
    ## Observed
    # zlimits[350]= 0.48428984672318626
    # zlimits[450]= 0.5153965525229397
    # zlimits[550]= 0.457601519681116
    zlimits[350]=0.44998454896736934
    zlimits[450]=0.5786885207831626
    zlimits[550]=0.3976499391742974

    g=ROOT.TGraph()
    g_core=ROOT.TGraph()
    g_zprime=ROOT.TGraph()    
    gidx=0
    for d_sig in d_sigs:
        mR,mDM,gSM,gDM=zprimetools.info(os.path.basename(d_sig[0].GetFile().GetPath()[:-2]))        
        acc,acc_core=extractPeak(d_sig)        
        print(acc)

        g     .SetPoint(gidx,mR,acc)
        g_core.SetPoint(gidx,mR,acc_core)

        if mR in zlimits:
            g_zprime.SetPoint(gidx,mR,zlimits[mR]*acc_core)

        gidx+=1
    plottools.graphs([(g,{'title':'All m_{jj}'}),(g_core,{'title':'Fit Range m_{jj}','color':ROOT.kRed})],xtitle='m_{R} [GeV]',
                     ytitle='Fraction of Gaussian core',
                     legend=(0.6,0.8),
                     yrange=(0,1))
    canvastools.save('fraction.pdf')

    plottools.graphs([(g_gaussian,{'title':'7% Gaussian'}),(g_zprime,{'title':'Z\'','color':ROOT.kRed})],xtitle='m_{R} [GeV]',
                     ytitle='Limit [pb]',
                     yrange=(1e-2,10),
                     legend=(0.3,0.8),
                     opt='PL',
                     logy=True)
    canvastools.save('limit.pdf')


def main(sigDir,**kwargs):
    canvastools.savesetup('extractPeak',**kwargs)

    signals=glob.glob('%s/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mR*_mD10_gSp3_gD1.dijet.*_tree.root.root'%sigDir)
    signals.reverse()
    print(signals)
    selectiontools.loop(extractPeaks,*signals,**kwargs)

    canvastools.savereset()
