import ROOT

from prot import filetools

def makeDataFrame(path,treeName,isMC=False):
    fh=filetools.open(path)
    tree=fh.Get(treeName)

    df=ROOT.Experimental.TDataFrame(tree)

    # Add nEvents normalization
    df_w=None
    if isMC:
        cutflow=fh.Get('cutflow_Test')
        nEvents=cutflow.GetBinContent(1)
        df_w=df.Define('w','weight/%d'%nEvents)
    else:
        df_w=df.Define('w','weight')

    return df_w
