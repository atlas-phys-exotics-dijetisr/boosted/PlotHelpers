from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import utiltools
from prot import fittools

import timeit

import fitdatamctools

from hbbisr import funcfactory
from hbbisr import hbbfittools
from hbbisrnote import constants

import ROOT

import sys

ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize")
ROOT.Math.MinimizerOptions.SetDefaultMaxIterations(1000000)
ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
ROOT.Math.MinimizerOptions.SetDefaultTolerance(0.1)

def main(selectioncode='cr',year=15,fname='exppol4',mrange=(70,230)):
    #
    # Create histograms
    fh_data =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_data/hist-data{}.root'.format(year))

    fh_w    =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-w.root')
    fh_z    =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-z.root')
    fh_v    =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-v.root')
    fh_qcd  =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-qcd.root')
    fh_ttbar=filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-ttbar.root')

    selection=''
    if selectioncode=='cr':
        selection=constants.selectionCR.replace('_hm40','')
        title='CR - Data {}'.format(year)
    elif selectioncode=='vr':
        selection=constants.selectionVR.replace('_hm40','')
        title='VR - Data {}'.format(year)
    elif selectioncode=='sr':
        selection=constants.selectionSR.replace('_hm40','')
        title='SR - Data {}'.format(year)
    else:
        print('Unknown selection code {}'.format(selectioncode))
        return

    h_data =fh_data .Get('{}/Hcand_m_s'.format(selection))

    h_w    =fh_w    .Get('{}/Hcand_m_s'.format(selection))
    h_z    =fh_z    .Get('{}/Hcand_m_s'.format(selection))
    h_v    =fh_v    .Get('{}/Hcand_m_s'.format(selection))
    h_qcd  =fh_qcd  .Get('{}/Hcand_m_s'.format(selection))
    h_ttbar=fh_ttbar.Get('{}/Hcand_m_s'.format(selection))

    h_v    .Scale(getattr(constants,'lumi{}'.format(year))*1e3)
    h_qcd  .Scale(getattr(constants,'lumi{}'.format(year))*1e3)
    h_ttbar.Scale(getattr(constants,'lumi{}'.format(year))*1e3)

    #
    # Prepare fit functions
    f_qcd=funcfactory.funcs['f'+fname]
    fitnames=['qcd','v','ttbar']
    fitwiths=[f_qcd,h_v,h_ttbar]
    fitinits=[f_qcd.GetParameter(i) for i in range(f_qcd.GetNpar())]+[1.,1.]
    muvidx=len(fitinits)-2
    muttbaridx=len(fitinits)-2

    # Fit the data
    start_time = timeit.default_timer()
    result=hbbfittools.fitDataMC(fitwiths,h_data,mrange=mrange,guess=fitinits)
    elapsed = timeit.default_timer() - start_time
    print(elapsed)

    # Get other information
    chi2=result.MinFcnValue()
    ndof=(h_data.FindBin(mrange[1])-h_data.FindBin(mrange[0]))-1-7
    prob=ROOT.TMath.Prob(chi2,ndof)

    # Plot the fit result
    h_qcd=h_data.Clone()
    utiltools.store.append(h_qcd)
    h_qcd.Reset()
    for binidx in range(h_qcd.FindBin(mrange[0]),h_qcd.FindBin(mrange[1])):
        val=f_qcd.Integral(h_qcd.GetBinLowEdge(binidx),h_qcd.GetBinLowEdge(binidx+1))/h_qcd.GetBinWidth(binidx)
        h_qcd.SetBinContent(binidx,val)
    
    plottools.plots([(h_qcd,{'title':'QCD',
                             'fillcolor':ROOT.UCYellow,
                             'opt':'hist'}),
                     (h_v,{'title':'V+jets (#mu={:0.2f}#pm{:0.2f})'.format(result.Parameter(muvidx),result.ParError(muvidx)),
                           'fillcolor':ROOT.kRed,
                           'opt':'hist',
                           'scale':result.Parameter(muvidx)}),
                     (h_ttbar,{'title':'t#bar{{t}} (#mu={:0.2f}#pm{:0.2f})'.format(result.Parameter(muttbaridx),result.ParError(muttbaridx)),
                               'fillcolor':ROOT.kGreen+2,
                               'opt':'hist',
                               'scale':result.Parameter(muttbaridx)}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2),3),
                    text='{title}\n#chi^{{2}}={:0.2f} p={:0.2f}'.format(chi2,prob,title=title),lumi=getattr(constants,'lumi{}'.format(year)),sim=False,textpos=(0.5,0.65),
                    legend=(0.5,0.93),legendncol=2,
                    ytitle='Events',                    
                    xrange=mrange,
                    ratiomode='resid',ratio=0)
    canvastools.save('{}/data{}.pdf'.format(selectioncode,year))

    print([result.Parameter(i) for i in range(f_qcd.GetNpar())])
