import ROOT

from prot import plottools
from prot import utiltools
from prot import treetools
from prot import filetools
from prot import canvastools
from prot import style

def main(rootFile,title):
    filetools.filemap(rootFile,"info")

    gSMs=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
    mRs=[100,200,300,400,500,600,700,800,900]

    gs_xsecvsmR =[]
    gs_xsecvsgSM=[]

    gs_widthvsmR =[]
    gs_widthvsgSM=[]

    gs_filteffvsmR =[]
    gs_filteffvsgSM=[]

    #
    # vs mR
    for gSM in gSMs:
        color=style.palette[int(gSM*10-1)]

        g_xsecvsmR=treetools.treeGraph("info:/tree",
                                       "mR",
                                       "xsec",
                                       "gSM==%0.1f"%gSM,
                                       xtitle="m_{R} [GeV]",ytitle="cross-section (pb)",title='%0.1f'%gSM,markercolor=color)
        gs_xsecvsmR.append(g_xsecvsmR)

        g_widthvsmR=treetools.treeGraph("info:/tree",
                                        "mR",
                                        "width/mR",
                                        "gSM==%0.1f"%gSM,
                                        xtitle="m_{R} [GeV]",ytitle="#Gamma/m_{R}",title='%0.1f'%gSM,markercolor=color)
        gs_widthvsmR.append(g_widthvsmR)

        g_filteffvsmR=treetools.treeGraph("info:/tree",
                                          "mR",
                                          "filteff",
                                          "gSM==%0.1f"%gSM,
                                          xtitle="m_{R} [GeV]",ytitle="Filter Efficiency",title='%0.1f'%gSM,markercolor=color)
        gs_filteffvsmR.append(g_filteffvsmR)

    plottools.graphs(gs_xsecvsmR,mgopt='ACP',
                     #yrange=(1e0,1e6),xrange=(0,1000),
                     yrange=(1e-2,1e4),xrange=(0,1000),
                     logy=True,
                     text='%s\nLegend is for g_{SM}'%title,textpos=(0.55,0.73),
                     legend=(0.2,0.35),legendncol=3,legendwidth=0.1)
    canvastools.save("info_xsecvsmR.pdf")

    plottools.graphs(gs_widthvsmR,mgopt='ACP',
                     yrange=(0,0.55),xrange=(0,1000),
                     text='%s\nLegend is for g_{SM}'%title,textpos=(0.2,0.75),
                     legend=(0.55,0.9),legendncol=3,legendwidth=0.1)
    canvastools.save("info_widthvsmR.pdf")

    plottools.graphs(gs_filteffvsmR,mgopt='ACP',
                     yrange=(0,1.),xrange=(0,1000),
                     text='%s\nLegend is for g_{SM}'%title,textpos=(0.2,0.75),
                     legend=(0.55,0.9),legendncol=3,legendfilteff=0.1)
    canvastools.save("info_filteffvsmR.pdf")

    #
    # vs gSM
    for mR in mRs:
        color=style.palette[int(mR/100-1)]

        g_xsecvsgSM=treetools.treeGraph("info:/tree",
                                        "gSM",
                                        "xsec",
                                        "mR==%d"%mR,
                                        xtitle="g_{SM}",ytitle="cross-section (pb)",title='%d GeV'%mR,markercolor=color)
        gs_xsecvsgSM.append(g_xsecvsgSM)

        g_widthvsgSM=treetools.treeGraph("info:/tree",
                                        "gSM",
                                        "width/mR",
                                        "mR==%d"%mR,
                                        xtitle="g_{SM}",ytitle="#Gamma/m_{R}",title='%d GeV'%mR,markercolor=color)
        gs_widthvsgSM.append(g_widthvsgSM)

        g_filteffvsgSM=treetools.treeGraph("info:/tree",
                                          "gSM",
                                          "filteff",
                                          "mR==%d"%mR,
                                          xtitle="g_{SM}",ytitle="Filter Efficiency",title='%d GeV'%mR,markercolor=color)
        gs_filteffvsgSM.append(g_filteffvsgSM)

    plottools.graphs(gs_xsecvsgSM,mgopt='ACP',
                     yrange=(1e0,1e7),xrange=(0,1),
                     logy=True,
                     text='%s\nLegend is for m_{R}'%title,textpos=(0.19,0.77),
                     legend=(0.5,0.35),legendncol=3,legendwidth=0.15)
    canvastools.save("info_xsecvsgSM.pdf")

    plottools.graphs(gs_widthvsgSM,mgopt='ACP',
                     yrange=(0,0.55),xrange=(0,1),
                     text='%s\nLegend is for m_{R}'%title,textpos=(0.2,0.75),
                     legend=(0.2,0.7),legendncol=3,legendwidth=0.15)
    canvastools.save("info_widthvsgSM.pdf")

    plottools.graphs(gs_filteffvsgSM,mgopt='ACP',
                     yrange=(0,1.),xrange=(0,1),
                     text='%s\nLegend is for m_{R}'%title,textpos=(0.2,0.75),
                     legend=(0.55,0.9),legendncol=3,legendfilteff=0.15)
    canvastools.save("info_filteffvsgSM.pdf")

    #
    # 2D plots
    treetools.treePlot2D("info:/tree",
                         "mR",10,50,1050,
                         "gSM",9,0.05,0.95,
                         "xsec * (gSM!=0.16)",
                         "h2_xsec",
                         opt='COLZ TEXT',
                         xtitle='m_{R} [GeV]',ytitle='g_{SM}',ztitle='cross-section (pb)',
                         zrange=(5e-2,2e3),
                         logz=True)
    canvastools.save("info_xsec.pdf")

    ROOT.gStyle.SetPaintTextFormat("0.2f")
    treetools.treePlot2D("info:/tree",
                         "mR",10,50,1050,
                         "gSM",9,0.05,0.95,
                         "width/mR * (gSM!=0.16)",
                         "h2_width",
                         opt='COLZ TEXT',
                         xtitle='m_{R} [GeV]',ytitle='g_{SM}',ztitle='#Gamma/m_{R}',
                         zrange=(0,0.55),
                         logz=True)
    canvastools.save("info_width.pdf")

    treetools.treePlot2D("info:/tree",
                         "mR",10,50,1050,
                         "gSM",9,0.05,0.95,
                         "filteff * (gSM!=0.16)",
                         "h2_filteff",
                         opt='COLZ TEXT',
                         xtitle='m_{R} [GeV]',ytitle='g_{SM}',ztitle='Filter Efficiency',
                         zrange=(0,1.),
                         logz=True)
    canvastools.save("info_filteff.pdf")
