import ROOT

import re

binsDijetDefault = [ 203, 216, 229, 243, 257, 272, 287, 303, 319, 335, 352, 369, 387, 405, 424, 443, 462, 482, 502, 523, 544, 566, 588, 611, 634, 657, 681, 705, 730, 755, 781, 807, 834, 861, 889, 917, 946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100, 10292, 10488, 10688, 10892, 11100, 11312, 11528, 11748, 11972, 12200, 12432, 12669, 12910, 13156 ]

func3par=ROOT.TF1("dijet3par","[0]*((1-x/13000)^[1])*((x/13000)^(-[2]))",0,10e3)
func4par=ROOT.TF1("dijet4par","[0]*((1-x/13000)^[1])*((x/13000)^(-[2]+[3]*log(x/13000)))",0,10e3)
func5par=ROOT.TF1("dijet5par","[0]*((1-x/13000)^[1])*((x/13000)^(-[2]+[3]*log(x/13000)-[4]*log(x/13000)^2))",0,10e3)

def lumistr(lumi):
    return ('%0.2f'%lumi).replace('.','p')

def lumiint(lumistr):
    if lumistr==None: return None
    return float(lumistr.replace('p','.').replace('fb',''))

re_datalike     =re.compile('Zprime_mjj_var_DataLike_([0-9]+p[0-9]+)fb')
re_scaled       =re.compile('Zprime_mjj_var_Scaled_([0-9]+p[0-9]+)fb')
re_fitresultLogL=re.compile('FitResultLogL_(.+)_Zprime_mjj_var_DataLike_([0-9]+p[0-9]+fb)')
re_fitresultChi2=re.compile('FitResultChi2_(.+)_Zprime_mjj_var_DataLike_([0-9]+p[0-9]+fb)')

#re_datalike     =re.compile('m12_DataLike_SinglePhoton_([0-9]+p?[0-9]*)fb')
#re_fitresultLogL=re.compile('FitResultLogL_(.+)_m12_DataLike_SinglePhoton_([0-9]+p?[0-9]*fb)')
#re_fitresultChi2=re.compile('FitResultChi2_(.+)_m12_DataLike_SinglePhoton_([0-9]+p?[0-9]*fb)')

class DijetItem:
    def __init__(self,selection,lumi):
        self.selection=selection
        self.lumi=lumi
        self.h_datalike=None
        self.h_scaled=None

        self.fitresultLogL={}
        self.fitresultChi2={}

class DijetData:
    def __init__(self,fh):
        self.selections={}   # sort by selection
        self.luminosities={} # sort by luminosity

        for key in fh.GetListOfKeys():
            selection=key.GetName()
            seldir=key.ReadObj()

            self.selections[selection]={}
            self.load_selection(selection,seldir)
            for selection,lumis in self.selections.items():
                for lumi,item in lumis.items():
                    if lumi not in self.luminosities: self.luminosities[lumi]={}
                    self.luminosities[lumi][selection]=item

    def load_selection(self,selection,seldir):
        for key in seldir.GetListOfKeys():
            name=key.GetName()

            # This is a datalike hist!
            match=re_datalike.match(name)
            if match!=None:
                lumi=lumiint(match.group(1))
                item=self.create_item(selection,lumi)
                item.h_datalike=key.ReadObj()

            # This is a scaled hist!
            match=re_scaled.match(name)
            if match!=None:
                lumi=lumiint(match.group(1))
                item=self.create_item(selection,lumi)
                item.h_scaled=key.ReadObj()

            # This is a fitresultLogL hist!
            match=re_fitresultLogL.match(name)
            if match!=None:
                func=match.group(1)
                lumi=lumiint(match.group(2))
                item=self.create_item(selection,lumi)
                item.fitresultLogL[func]=key.ReadObj()

            # This is a fitresultChi2 hist!
            match=re_fitresultChi2.match(name)
            if match!=None:
                func=match.group(1)
                lumi=lumiint(match.group(2))
                item=self.create_item(selection,lumi)
                item.fitresultChi2[func]=key.ReadObj()
                
    def create_item(self,selection,lumi):
        seldata=self.selections[selection]
        if lumi not in seldata:
            item=DijetItem(selection,lumi)
            seldata[lumi]=item
        return seldata[lumi]
