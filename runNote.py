import plot_datamc
import makeAcceptanceTable
import plot_xseccomp
import cutStudy
import makeDataLike
from samplevalidation import plot_sampleinfo

from prot import style
from prot import utiltools

import ROOT

def main(doDatalike=True):
    #
    # Cross-sections
    #plot_sampleinfo.main("samplevalidation/dijetgamma.root","Z' + #gamma (P_{T,#gamma} > 10 GeV)")
    #plot_sampleinfo.main("samplevalidation/mergetrijet.root","Z' + j (P_{T,j} > 20 GeV)")

    # Get acceptances
    #makeAcceptanceTable.main('OUT_dijetjet_truth/hist-MC15.999821.MGPy8EG_dmA_dijet_Np1_Jet350*.root','xsec_dijetjet_sel.root')

    # x-sec comparison
    plot_xseccomp.main([("data/xsec/Zp.root:/tree",{'title':'#sigma'}),
                        ("data/xsec/Zpj_ptj430.root:/tree",{'title':'#sigma + j (P_{T}>430 GeV)',
                                                     'color':ROOT.kRed}),
                        ("data/xsec/Zpa_pta150.root:/tree",{'title':'#sigma + #gamma (P_{T}>150 GeV)',
                                                            'color':ROOT.kBlue})])

    plottools.plots([('_file0:/Zprime_mjj_combined',{'title':'Di-Jet',
                                                     'scale':9.5/3.20}),
                     ('_file1:/trijet_j430_2j25_nomjj/Zprime_mjj',{'rebin':2,
                                                                   'title':'Di+Jet + jet (p_{T} > 430 GeV)',
                                                                   'color':ROOT.kRed}),
                     ('_file2:/dijetgamma_g150_2j25_nomjj/Zprime_mjj',{'rebin':2,
                                                                       'title':'Di+Jet + #gamma (p_{T} > 150 GeV)',
                                                                       'color':ROOT.kBlue})]
                    ,xrange=(200,1000),logy=True,ytitle='Events',hsopt='nostack',legend=(0.5,0.92),text='',lumi=9.5,textpos=(0.2,0.17),ratio=0,ratiotitle='Ratio',ratiology=True,yrange=(1e1,1e10),ratiorange=(1e-5,1e-2))
