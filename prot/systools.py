import ROOT

from prot import style

import fnmatch
from math import *

def get_syslist(hist,sys=False,rebin=None):
    if sys==False: return []
    if hist.GetDirectory()==None: return []

    systs={}
    for key in hist.GetDirectory().GetListOfKeys():
        if not key.ReadObj().InheritsFrom(ROOT.TDirectoryFile.Class()): continue
        dirobj=key.ReadObj()
        name=key.GetName()
        if not name.endswith('1down') and not name.endswith('1up'): continue

        sysobj=dirobj.Get(hist.GetName())
        if sysobj==None: continue

        sysname=name[3:].split('__')[0]
        if type(sys)==str and not fnmatch.fnmatch(sysname,sys): continue
        if type(sys)==list and sysname not in sys: continue

        if sysname not in systs: systs[sysname]=[]

        if rebin!=None:
            sysobj=sysobj.Clone()
            if type(rebin)==int:
                sysobj.Rebin(rebin)
            else:
                sysobj=histtools.variable_rebin(sysobj,rebin)
            
        systs[sysname].append(sysobj)

    for key in systs:
        systs[key]=tuple(systs[key])
    return systs

def apply_systematics(hist,syslist,symmetric=False):
    binRange=range(1,hist.GetNbinsX()+1)

    gsys=ROOT.TGraphAsymmErrors()
    gidx=0
    for binIdx in binRange:
        # Values
        x=hist.GetBinCenter(binIdx)
        val=hist.GetBinContent(binIdx)
        minerrerr=0
        maxerrerr=0

        # Loop over syslist combinations
        for systuple in syslist:
            errs=[val-syshist.GetBinContent(binIdx) for syshist in systuple]+[0]
            newminerr=min(errs)
            newmaxerr=max(errs)
            minerrerr+=newminerr*newminerr
            maxerrerr+=newmaxerr*newmaxerr

        # Symmetrize
        if symmetric:
            minerrerr=max(minerrerr,maxerrerr)
            maxerrerr=max(minerrerr,maxerrerr)

        # Save error
        width=hist.GetBinWidth(binIdx)/2
        gsys.SetPoint(gidx,x,val)
        gsys.SetPointError(gidx,width,width,sqrt(minerrerr),sqrt(maxerrerr))
        gidx+=1

    gsys.SetMarkerStyle(0)
    gsys.SetFillStyle(3002)
    gsys.SetFillColor(ROOT.kRed)
    return gsys
