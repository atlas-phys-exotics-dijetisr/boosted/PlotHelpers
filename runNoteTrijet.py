import glob
import os, os.path

import plot_datamc
import makeAcceptanceTable
import plot_xseccomp
import cutStudy
import addDataHists
import runSingleFit
import makeSysInputs
import plotSingleFit
import plotFitInfo
import quickLimits
import table_cutflow
import table_acceff
import makeDataLike
import plot_accuncert
import plot_accuncert_jesscenarios
import plot_acceptances

from samplevalidation import plot_sampleinfo

from prot import style
from prot import utiltools
from prot import triggertools
from prot import filetools
from prot import plottools
from prot import canvastools

import zprimetools

import ROOT

def runTriggerTurnOn():
    fh_data15=filetools.open('OUT_dijetjet_trigger_data/hist15.root')
    fh_data16=filetools.open('OUT_dijetjet_trigger_data/hist16.root')
    fh_mc    =filetools.open('OUT_dijetjet_trigger_mc/hist.root')

    data15=triggertools.trigeff(fh_data15.Get('trijet_j0_2j25_num/jet0_pt_m'),fh_data15.Get('trijet_j0_2j25_den/jet0_pt_m'),xrange=(320,550)).CreateGraph()
    data16=triggertools.trigeff(fh_data16.Get('trijet_j0_2j25_num/jet0_pt_m'),fh_data16.Get('trijet_j0_2j25_den/jet0_pt_m'),xrange=(320,550)).CreateGraph()
    mc    =triggertools.trigeff(fh_mc    .Get('trijet_j0_2j25_num/jet0_pt_m'),fh_mc    .Get('trijet_j0_2j25_den/jet0_pt_m'),xrange=(320,550)).CreateGraph()

    plottools.graphs([(mc,{'title':'Pythia 8 Dijet','color':ROOT.kRed,'markerstyle':ROOT.kOpenTriangleUp}),
                      (data15,{'title':'2015 Data'}),
                      (data16,{'title':'2016 Data','markerstyle':ROOT.kOpenCircle})],
                      xrange=(320,550),
                      legend=(0.6,0.4),
                      text='HLT_j380 Turn-On',
                      textpos=(0.55,0.6),
                      sim=False)

    canvastools.save('trigger_HLT_j380.pdf')

def runCutflow():
    table_cutflow.main("OUT_dijetjet_data/hist.root"     ,cfname='dijetjet_data')
    table_cutflow.main("OUT_dijetjet_mc/hist.root"       ,cfname='dijetjet_mc'      ,scale=15.45e3*0.83)
    table_cutflow.main("OUT_dijetjet_mc/hist_Sherpa.root",cfname='dijetjet_mcsherpa',scale=15.45e3*0.98)

    # signals
    table_cutflow.main("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305540.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",cfname='dijetjet_sigThreeHundredFifty', scale=15.45e3)
    table_cutflow.main("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",cfname='dijetjet_sigFourHundredFifty' , scale=15.45e3)
    table_cutflow.main("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305548.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",cfname='dijetjet_sigFiveHundredFifty' , scale=15.45e3)

def runAcceptance():
    makeAcceptanceTable.main('OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_*.root','OUT_dijetjet_mc/signal_acceptances.root',truthpath='OUT_dijetjet_truth')
    plot_acceptances.main('OUT_dijetjet_mc/signal_acceptances.root')
    table_acceff.main('OUT_dijetjet_mc/signal_acceptances.root',aename='dijetjet')

def runDataMC():
    #
    # Data vs MC comparisons
    oldstyledata=style.style.data
    style.style.parse('styles/variables.style')

    plot_datamc.main(("OUT_dijetjet_mc/hist.root",{"title":"Pythia Dijet"}),
                     ("OUT_dijetjet_data/hist.root",{"title":"Data"}),
                     ("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305540.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{"title":"m_{R}=350 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}),
                     #("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{"title":"m_{R}=450 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}),
                     ratiorange=(0.5,1.5),ytitle="Events",text='',lumi=15.45,sim=False,selection='trijet_j430_2j25*',outName='pythia8')

    plot_datamc.main(("OUT_dijetjet_mc/hist.root",{"title":"Pythia Dijet"}),
                     ("OUT_dijetjet_data/hist.root",{"title":"Data"}),
                     ("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305548.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{"title":"m_{R}=550 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}),
                     #("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{"title":"m_{R}=450 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}),                     
                     ratiorange=(0.5,1.5),ytitle="Events",text='',lumi=15.45,sim=False,selection='dijet13_j430_j25',outName='pythia8')

    plot_datamc.main(("OUT_dijetjet_mc/hist_Sherpa.root",{"title":"Sherpa Multijet"}),
                     ("OUT_dijetjet_data/hist.root",{"title":"Data"}),
                     ("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305540.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{"title":"m_{R}=350 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}), 
                     #("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{"title":"m_{R}=450 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}),                    
                     ratiorange=(0.5,1.5),ytitle="Events",text='',lumi=15.45,sim=False,selection='trijet_j430_2j25',outName='sherpa')

    style.style.data=oldstyledata

def runCutStudy():
    style.style.parse('styles/cutvariables.style')

    cutStudy.main("OUT_dijetjet_mc/hist.root",
                  [("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305540.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{'title':'m_{R}=350 GeV','linecolor':ROOT.kBlue}),
                   ("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{'title':'m_{R}=450 GeV','linecolor':ROOT.kRed})],
                   lumi=15.45e3,
                   selection='trijet_j430_2j25*',outName='pythia8')

    cutStudy.main("OUT_dijetjet_mc/hist_Sherpa.root",
                  [("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305540.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{'title':'m_{R}=350 GeV','linecolor':ROOT.kBlue}),
                   ("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.dijet.20160723-01_tree.root.root",{'title':'m_{R}=450 GeV','linecolor':ROOT.kRed})],
                   lumi=15.45e3,texprefix='sherpa',
                   selection='trijet_j430_2j25*',outName='sherpa')

def runDatalike():
    outLumis=[0.1,0.3,0.5,0.7,0.9,1.,3.,3.21,5.,7.,9.,10.,12.,12.24,14.,15.45,16.,18.,20.]

    makeDataLike.main('OUT_dijetjet_mc/hist.root',outLumis=outLumis)
    makeDataLike.main('OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.*.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*[0-9]W.dijet.20160722-01_tree.root.root',outLumis=outLumis,split='hist-split.root')

    makeDataLike.main('OUT_dijetjet_mc/hist_JZ.root',outLumis=outLumis)
    makeDataLike.main('OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.*.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*[0-9].dijet.20160722-01_tree.root.root',outLumis=outLumis,split='hist_JZ-split.root')    

    makeDataLike.main('OUT_dijetjet_mc/hist_Sherpa.root',outLumis=outLumis)
    makeDataLike.main('OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.*.Sherpa_CT10_jets_JZ*[0-9].dijet.20160722-01_tree.root.root',outLumis=outLumis,split='hist_Sherpa-split.root')

    makeDataLike.main('OUT_dijetjet_tla/hist-user.kkrizka.mc15_13TeV.304784.Pythia8EvtGen_A14NNPDF23LO_jetjet_Powerlaw.dijet.20160722-01_tree.root.root',outLumis=outLumis)

    signals=glob.glob('OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_*.dijet.20160723-01_tree.root.root')
    for signal in signals:
        newname=zprimetools.nameTag(os.path.basename(signal))
        makeDataLike.main(signal,nameTag=newname,outLumis=outLumis)

    addDataHists.main("OUT_dijetjet_data")

def runFits():
    runSingleFit.main("OUT_dijetjet_tla/datalike/hist-user.kkrizka.mc15_13TeV.304784.Pythia8EvtGen_A14NNPDF23LO_jetjet_Powerlaw.dijet.20160606-01_tree.root.root",outFile='OUT_dijetjet_tla/datalike/fit.root',massRanges=(303,611))
    plotSingleFit.main("OUT_dijetjet_tla/datalike/fit.root",outName='mc',dijetard=True,massRanges=(303,611))
    plotFitInfo.main("OUT_dijetjet_tla/datalike/fit.root",outName='mc')

    runSingleFit.main("OUT_dijetjet_data/datalike.root",outFile='OUT_dijetjet_data/fit.root',massRanges=(303,611))
    plotSingleFit.main("OUT_dijetjet_data/fit.root",outName='data',dijetard=True,massRanges=(303,611),sim=False)
    plotFitInfo.main("OUT_dijetjet_data/fit.root",outName='data',sim=False)

def runAccUncertainty():
    style.style.parse('styles/systematics.style')
    style.style.parse('styles/jesscenarios.style')

    allsignals=[]
    for scenario in range(1,2):
        signals=glob.glob("OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_*gSp3*.dijet.*-0%d_tree.root.root"%scenario)

        newsignals=[]
        for signal in signals:
            mR,mDM,gSM,gDM=zprimetools.info(signal)
            if mR<250 or 1500<=mR: continue
            allsignals.append((signal,{'mR':mR,'scenario':scenario}))
            newsignals.append((signal,{'mR':mR}))

        if scenario==1:
            plot_accuncert.main(allsignals,textpos=(0.6,0.8),text='')
    plot_accuncert_jesscenarios.main(allsignals,textpos=(0.6,0.8),text='')
    
def runPrepSysFiles():
    signals=glob.glob("OUT_dijetjet_sysmc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_*.dijet.*_tree.root.root")
    for signal in signals:
        newname=zprimetools.nameTag(os.path.basename(signal))
        makeSysInputs.main(signal,histname='Zprime_mjj_var',nameTag=newname)    

def run2DLimits():
    quickLimits.main("data/limits_j430_2j25.txt",    lumi=15.45,         selection='trijet_j430_2j25',outfile='trijet_j430_2j25/Zprime2D_data.pdf')
    quickLimits.main("data/limits_j430_2j25_mc.txt", lumi=15.45,sim=True,selection='trijet_j430_2j25',outfile='trijet_j430_2j25/Zprime2D_mc.pdf')

def main(doTriggerTurnOn=False,doCutflow=False,doAcceptance=False,doDataMC=False,doCutStudy=False,doDatalike=False,doFits=False,doAccUncertainty=False,doPrepSysFiles=False,do2DLimits=False):
    #
    # Make the trigger turn on
    if doTriggerTurnOn:
        runTriggerTurnOn()
    #
    # Make cutflow table
    if doCutflow:
        runCutflow()

    #
    # Make acceptance table
    if doAcceptance:
        runAcceptance()

    #
    # Do data vs MC comparisons
    if doDataMC:
        runDataMC()

    #
    # Do cut optimizations
    if doCutStudy:
        runCutStudy()

    #
    # Make datalike statistics
    if doDatalike:
        runDatalike()

    #
    # Run fit tests
    if doFits:
        runFits()

    #
    # Calculate simple acceptance uncertainties
    if doAccUncertainty:
        runAccUncertainty()
        
    #
    # Prepare systematic files
    if doPrepSysFiles:
        runPrepSysFiles()

    #
    # Run gSM limits
    if do2DLimits:
        run2DLimits()
