import ROOT

from prot import plottools
from prot import utiltools
from prot import treetools
from prot import filetools
from prot import canvastools
from prot import style

import selectiontools

def plot_acc(d,title='',**kwargs):
    gSMs=set()
    mRs=set()
    for e in d[0].Get('tree'):
        gSMs.add(e.gSM)
        mRs .add(e.mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))

    gs_seleffvsmR =[]
    gs_effvsmR    =[]    
    gs_seleffvsgSM=[]

    #
    # vs mR
    for gSM in gSMs:
        color=style.palette[int(gSM*10-1)]

        g_seleffvsmR=treetools.treeGraph(d[0].Get('tree'),
                                       "mR",
                                       "seleff",
                                       "gSM==%0.1f"%gSM,
                                       xtitle="m_{Z\'} [GeV]",ytitle="A #times #epsilon",title='%0.1f'%gSM,color=color)
        gs_seleffvsmR.append(g_seleffvsmR)

        g_effvsmR=treetools.treeGraph(d[0].Get('tree'),
                                       "mR",
                                       "eff",
                                       "gSM==%0.1f"%gSM,
                                       xtitle="m_{Z\'} [GeV]",ytitle="#epsilon",title='%0.1f'%gSM,color=color)
        gs_effvsmR.append(g_effvsmR)

    plottools.graphs(gs_seleffvsmR,mgopt='ACP',
                     yrange=(0,0.5),xrange=(mRs[0],mRs[-1]),
                     text='%s\nLegend is for g_{q}'%title,textpos=(0.5,0.75),
                     legend=(0.5,0.35),legendncol=3,legendwidth=0.1)
    canvastools.save("info_seleffvsmR.pdf")

    plottools.graphs(gs_effvsmR,mgopt='ACP',
                     yrange=(0.5,1.0),xrange=(mRs[0],mRs[-1]),
                     text='%s\nLegend is for g_{q}'%title,textpos=(0.2,0.25),
                     legend=(0.7,0.35),legendncol=2,legendwidth=0.1)
    canvastools.save("info_effvsmR.pdf")

    #
    # vs gSM
    for mR in mRs:
        g_seleffvsgSM=treetools.treeGraph(d[0].Get('tree'),
                                        "gSM",
                                        "seleff",
                                        "mR==%d"%mR,
                                          xtitle="g_{q}",ytitle='A #times #epsilon',title='%d GeV'%mR,style=str(mR))
        gs_seleffvsgSM.append(g_seleffvsgSM)

    plottools.graphs(gs_seleffvsgSM,mgopt='ACP',
                     yrange=(0,0.5),xrange=(gSMs[0],gSMs[-1]),
                     text='%s\nLegend is for m_{Z\'}'%title,textpos=(0.19,0.77),
                     legend=(0.5,0.35),legendncol=3,legendwidth=0.15)
    canvastools.save("info_seleffvsgSM.pdf")

    #
    # 2D plots
    ROOT.gStyle.SetPaintTextFormat("0.2f")    
    treetools.treePlot2D(d[0].Get('tree'),
                         "mR",10,50,1050,
                         "gSM",9,0.05,0.95,
                         "seleff",
                         "h2_seleff",
                         opt='COLZ TEXT',
                         xtitle='m_{Z\'} [GeV]',ytitle='g_{q}',ztitle='A #times #epsilon',
                         zrange=(0,0.5))
    canvastools.save("info_seleff.pdf")

def main(rootFile,**kwargs):
    canvastools.savesetup('acceff',**kwargs)

    # Prepare inputs
    rootFile=utiltools.filetag(rootFile)

    #
    selectiontools.loop(plot_acc,rootFile,**kwargs)

    canvastools.savereset()
