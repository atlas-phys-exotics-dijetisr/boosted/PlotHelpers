export WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export PYTHONPATH=${WORKDIR}:${PYTHONPATH}
alias prot='${WORKDIR}/prot.sh'
