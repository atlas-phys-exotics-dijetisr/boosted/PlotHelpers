import ROOT

from math import *
import glob

from prot import filetools
from prot import fittools
from prot import plottools
from prot import utiltools
from prot import canvastools
import zprimetools

def make_graph(bkgpath,siginputs,selection,lumi,massRange=None,sigscale=1.,bkgselection=None):
    # Get background
    bkgselection=selection if bkgselection==None else bkgselection
    h_bkg=utiltools.Get(bkgpath+':/'+bkgselection+'/Zprime_mjj')    

    # Get signal
    g=ROOT.TGraph()
    gidx=0
    files=glob.glob(siginputs)
    print('-')
    for sigpath in files:
        mR,mDM,gSM,gDM=zprimetools.info(sigpath)
        if massRange!=None and (mR<massRange[0] or massRange[1]<mR):
            continue

        h_sig=utiltools.Get(sigpath+':/'+selection+'/Zprime_mjj')

        # Determine fit range
        #m=mR #h_sig.GetMean()
        #s=h_sig.GetStdDev()
        #result=fittools.fit(h_sig,'gaus',fitrange=(m-s,m+s),xrange=(m-2*s,m+2*s))

        m=mR #result.Parameter(1)
        s=50 #result.Parameter(2)
        binmin=h_sig.FindBin(m-s)
        binmax=h_sig.FindBin(m+s)

        sig=h_sig.Integral(binmin,binmax)*sigscale
        bkg=h_bkg.Integral(binmin,binmax)

        print(mR,sig,bkg,gSM,lumi)
        
        # Calculate the limit
        lim=sqrt(2*sqrt(bkg)/sig*gSM**2/sqrt(lumi))
        g.SetPoint(gidx,mR,lim)
        gidx+=1

    return g

def main(lumi=3e3):
    lumi15=3212.96
    lumi16=32861.6

    g_dijetgamma_g150_2j25=make_graph('OUT_dijetgamma_mc/hist.root',
                                      'OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mR*_mD10_gSp3_gD1.gammajet.*_tree.root.root',
                                      'dijetgamma_g150_2j25', lumi15+lumi16, massRange=(160,1500))

    g_dijetgamma_g85_2j65=make_graph('OUT_dijetgamma_mc/hist.root',
                                      'OUT_dijetgamma_truth/hist-MC15.999999.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mR*_mD10_gSp25_gD1.NTUP.root',
                                     'dijetgamma_g85_2j65', lumi16, massRange=(200,1500), sigscale=0.8)

    g_trijet_j440_2j25=make_graph('OUT_dijetjet_mc/hist.root',
                                  'OUT_dijetjet_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mR*_mD10_gSp3_gD1.dijet.20170314-01_tree.root.root',
                                  'trijet_j440_2j25', lumi15+lumi16, massRange=(300,600), sigscale=0.8)
    
    plottools.graphs([(g_dijetgamma_g150_2j25,{'title':'dijetgamma_g150_2j25'}),
                      (g_dijetgamma_g85_2j65 ,{'title':'dijetgamma_g85_2j65','color':ROOT.kRed}),
                      (g_trijet_j440_2j25,{'title':'trijet_j440_2j25','color':ROOT.kBlue})],
                     ytitle='Limit on g_{SM}',xtitle='m_{R} [GeV]',yrange=(0,0.5),opt='C',legend=(0.6,0.9),xrange=(0,1000))
    canvastools.save('limit.pdf')
