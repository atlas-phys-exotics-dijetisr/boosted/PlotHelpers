#!/usr/bin/env python

import ROOT
import argparse
import os.path

def lsdir(d):
    results=[]
    for key in d.GetListOfKeys():
        obj=key.ReadObj()
        if obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            subresults=lsdir(obj)
            results+=['%s/%s'%(obj.GetName(),subresult) for subresult in subresults]
        else:
            results.append(obj.GetName())
    return results

parser=argparse.ArgumentParser(description='Merge directories in a ROOT files')
parser.add_argument('rootfile' , help='ROOT file containing directories')
parser.add_argument('inputdirs', nargs='+', help='List of directories to merge.')
parser.add_argument('outputdir', help='Name of output directory.')

args=parser.parse_args()

print(args.rootfile)
print(args.inputdirs)
print(args.outputdir)

f=ROOT.TFile(args.rootfile,'UPDATE')

if f.Get(args.outputdir)!=None:
    f.Delete(args.outputdir+';*')
d_out=f.mkdir(args.outputdir)

d_ref=f.Get(args.inputdirs[0].split(':')[0])
objpaths=lsdir(d_ref)
objects=[]
for objpath in objpaths:
    objdir=os.path.dirname(objpath)
    if d_out.Get(objdir)==None:
        d_out.mkdir(objdir)
    d_save=d_out.Get(objdir) if objdir!='' else d_out

    obj=d_ref.Get(objpath).Clone()
    obj.Reset()
    obj.SetDirectory(d_save)
    objects.append(obj)

    objlist=ROOT.TList()
    for inputdir in args.inputdirs:
        inputdir=inputdir.split(':')
        inobj=f.Get(inputdir[0]).Get(objpath).Clone()
        if len(inputdir)>1: inobj.Scale(float(inputdir[1]))
        objlist.Add(inobj)
    obj.Merge(objlist)
d_out.Write()
f.Close()

#dijet12_HLT_j15 dijet12_HLT_j25 dijet12_HLT_j35 dijet12_HLT_j60 dijet12_HLT_j110 dijet12_HLT_j175 dijet12_HLT_j260 dijet12_HLT_j360
