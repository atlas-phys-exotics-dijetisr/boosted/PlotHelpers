PlotHelpers (or prot for short) is a wrapper around PyROOT to make common plotting tasks easy.

# Instructions
PlotHelpers is best used through the `prot` command. It handles loading the different modules, setting up useful line magics, opening ROOT files passed as arguments and starting an IPython (if present) shell. Run `source setup.sh` to setup the command.

To start PlotHelpers, run `prot pathtorootfile.root`. This will result in a Python console with the `pathtorootfile.root` opened. Any number, including 0, of ROOT files can be given as the argument. The opened files are mapped to names `_fileN`, where N is their argument position starting at 0.

PlotHelpers takes advantage of ROOT's directory definitions to access histograms. For example, the `jet_pt` histogram in `_file0` is referred to as `_file0:/jet_pt`. If no file is specified, the path is assumed to be relative to the current directory. To list the contents of the current directory, use the `l` line-magic. To change the current directory, use the `c` line-magic with the path as an argument. To obtain a histogram, use the `Get` function from the `utiltools` module. If using the prot command line, the example is `Get('_file0:/jet_pt')`. Any ROOT objects can be accessed this way.

# Plotting
The three main plotting functions are `plot`, `plots` and `plotsf`. The are part of the `plottools` module. 

The `plot` function is designed to show a single histogram. As a result, its single argument being the histogram you want to draw. 

The `plots` function is designed to compare several histogram in a figure. It takes a single argument that is a list of histograms to plot in the same figure. The histograms can either be paths or `TH1` objects. If only two histograms are provided, they are shown with the `nostack` option and first histogram is given a yellow fill. Otherwise no style is applied.

The `plotsf` function is designed to compare histograms from several files with the same structure. It takes two arguments, with the first being a list of directories and the second being the name of a histogram contained in all of the directories. An example of the use is to compare the `jet_pt` distributions in a signal and background samples. If the ROOT file containing background histograms is `_file0` and signal histograms is `_file1`, then `plotsf` can be used as `plotsf(['_file0:/','_file1:/'],'jet_pt')`.

# Styles
The styling of histograms is done through keyword arguments. They are processed by the `style` module, which identifies the object type and sets the corresponding property. Most prot functions take the keyword arguments and apply them to all of the histograms being processed. To only change the attribute of a single histogram, specify it as a tuple with the second item being a dictionary with the properties.

The following show the example of using the styles to normalize two histograms and compare them.
```python
plotsf([('_file0:/',{'fillcolor':ROOT.kYellow}),'_file1:/'],'jet_pt',normalize=True,xtitle='jet p_{T} [GeV]',hsopt='nostack')
```

At this time, the list of possible properties can be obtained by looking at the `prot/style.py` file.

# Jupyter
PlotHelpers can also be used in Jupyter notebooks! In fact, there are examples of PlotHelpers in action at
https://swan.cern.ch/user/kkrizka/notebooks/PlotHelpers/Example.ipynb