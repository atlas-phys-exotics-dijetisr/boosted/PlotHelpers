#!/usr/bin/env python

from distutils.core import setup

setup(name='prot',
      version='0.1',
      description='A series of helper functions to make plotting ROOT histograms easier.',
      author='Karol Krizka',
      author_email='kkrizka@gmail.com',
      url='https://gitlab.cern.ch/atlas-phys-exotics-dijetisr/PlotHelpers',
      packages=['prot'],
      scripts=['prot.py']
      )
