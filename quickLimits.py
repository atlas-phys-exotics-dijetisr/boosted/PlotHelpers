import ROOT

from prot import utiltools
from prot import canvastools
from prot import plottools

import glob
import re
import os, os.path
import numpy as np
import array

from math import *

import selectiontools

def TextLabelBold(x,y,text="",color=ROOT.kBlack):
    l = ROOT.TLatex()
    #l.SetNDC()
    l.SetTextFont(62)
    l.SetTextSize(0.045)
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)

def bestbins(values,defaultwidth=50):
    mindiff=None
    for i in range(len(values)-1):
        diff=values[i+1]-values[i]
        if mindiff==None or diff<mindiff:
            mindiff=diff
    width=mindiff if mindiff!=None else defaultwidth
    minval=min(values)-width/2
    maxval=max(values)+width/2
    nbins=int(round((maxval-minval)/width))
    return minval,maxval,nbins

def main(inLimits,selection='dijetgamma_g130_2j25',lumi=None,sim=False,outfile='test.pdf'):
    # Load the limits
    limits={}
    fh=open(inLimits)
    limits = eval(fh.read())
    fh.close()

    # Table output
    fh_tex=canvastools.fopen('limits.tex','w')

    # Determine bins
    gSMs=set()
    mRs=set()
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))

    minmR ,maxmR ,nbinsmR =bestbins(mRs ,defaultwidth=50)
    mingSM,maxgSM,nbinsgSM=bestbins(gSMs,defaultwidth=0.1)
    maxgSM+=0.1
    nbinsgSM+=1

    # Make plot
    ratplots={}
    h2=ROOT.TH2F('excl','Exclusion Plot;m_{Z\'} [GeV];g_{q}',
                 nbinsmR,minmR,maxmR,
                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            binIdx=h2.FindBin(mR,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            h2.SetBinContent(binIdx,xslim_obs/xstheory)
            ratplots[(mR,gSM)]=(xslim_obs/xstheory,xslim_exp/xstheory)

            pname='mR%dgSM%0.2f'%(mR,gSM)
            pname=pname.replace('.','p')
            pname=pname.replace('0','Zero')
            pname=pname.replace('1','One')
            pname=pname.replace('2','Two')
            pname=pname.replace('3','Three')
            pname=pname.replace('4','Four')
            pname=pname.replace('5','Five')
            pname=pname.replace('6','Six')
            pname=pname.replace('7','Seven')
            pname=pname.replace('8','Eight')
            pname=pname.replace('9','Nine')

            fh_tex.write('\\newcommand{\LimitMCObs%s}{%0.2f}\n'%(pname,xslim_obs/xstheory))
            fh_tex.write('\\newcommand{\LimitMCExp%s}{%0.2f}\n'%(pname,xslim_exp/xstheory))

    fh_tex.close()
            

    # Make graph
    gobs=ROOT.TGraph()
    gexp=ROOT.TGraph()
    gexist=ROOT.TGraph()
    #gexist.SetMarkerColor(ROOT.kRed-9)
    gexist.SetMarkerStyle(20)
    gexist.SetMarkerSize(0.5)
    gidx=0
    gidxexist=0
    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            gexist.SetPoint(gidxexist,mR,gSM)
            gidxexist+=1
    
    for mR in mRs:
        # Get interesting point
        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR),h2.GetXaxis().FindBin(mR))
        binLim=None #h.GetNbinsX()
        for binIdx in range(h.GetNbinsX(),0,-1):
            mu=h.GetBinContent(binIdx)
            if mu==0: continue
            if mu>1 and binLim!=None:
                break            
            binLim=binIdx
        if binLim==None: continue
        gSMref=float('%0.3g'%h.GetBinCenter(binLim))

        limit=limits.get(gSMref,{}).get(mR,{})
        #gexist.SetPoint(gidx,mR,gSMref)

        xsref=limit['theory']
        xsobs=limit['obs']
        xsexp=limit['exp']
        gSMobs=0
        gSMexp=0
        if xsobs==None:
            continue
        else:
            gSMobs=sqrt(gSMref**2*xsobs/xsref)
            gSMexp=sqrt(gSMref**2*xsexp/xsref)

        print(mR,gSMref,gSMobs,xsobs,xsref)
        print(mR,gSMref,gSMexp,xsexp,xsref)
        gobs.SetPoint(gidx,mR,gSMobs)
        gexp.SetPoint(gidx,mR,gSMexp)
        gidx+=1            
    #gobs.SetPoint(gidx,mRs[-1]+50,gSMobs)
    #gexp.SetPoint(gidx,mRs[-1]+50,gSMexp)

    # Plot plot
    # FIATLAS=ROOT.TColor.CreateGradientColorTable(5,
    #                                              np.array([0.00, 0.16, 0.39, 0.66, 1.00],'d'),
    #                                              np.array([0.51, 1.00, 0.87, 0.00, 0.00],'d'),
    #                                              np.array([0.00, 0.20, 1.00, 0.81, 0.00],'d'),
    #                                              np.array([0.00, 0.00, 0.12, 1.00, 0.51],'d'),
    #                                              255
    #                                              )    
    FIATLAS=ROOT.TColor.CreateGradientColorTable(2,
                                                 np.array([0.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 255
                                                 )    

    ATPallete=array.array('i',range(FIATLAS,FIATLAS+255))

    ROOT.gStyle.SetPalette(255,ATPallete)
    
    c1=canvastools.canvas()
    c1.Clear()

    #l=ROOT.TLegend(0.2,0.3,0.5,0.2)
    l=ROOT.TLegend(0.2,0.92,0.5,0.8)
    # l.AddEntry(gobs,'#sigma_{obs. limit}/#sigma_{theory}=1'  ,'L')
    # l.AddEntry(gexp,'(#sigma_{exp. limit}/#sigma_{theory}=1)','L')
    l.AddEntry(gobs,'obs. limit','L')
    l.AddEntry(gexp,'exp. limit','LF')

    #plottools.plot2d(h2,text={'title':selection,'lumi':lumi},textpos=(0.38,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    plottools.plot2d(h2,text=selectiontools.titles.get(selection,selection),lumi=lumi,sim=sim,textpos=(0.45,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')

    gexist.SetMarkerSize(0)
    gexist.Draw('P SAME')
    # Tl=ROOT.TLatex()
    # Tl.SetTextColor(34)
    # Tl.SetTextSize(0.03)
    # for (mR,gSM),(xsobs,xsexp) in ratplots.items():
    #     print(mR,gSM)
    #     Tl.DrawLatex(mR-25,gSM+0.005,'%0.2f'  %(xsobs))
    #     Tl.DrawLatex(mR-25,gSM-0.025,'(%0.2f)'%(xsexp))

    dijetISRColor=ROOT.kCyan+1
    gobs.Draw('C SAME')
    gobs.SetLineWidth(2)
    gobs.SetLineColor(dijetISRColor)
    #gobs.SetFillStyle(3375)
    #gobs.SetFillColor(dijetISRColor)

    gexp.Draw('C SAME')
    gexp.SetLineWidth(505)
    gexp.SetLineColor(dijetISRColor)
    gexp.SetLineStyle(ROOT.kDashed)
    gexp.SetFillStyle(3005)
    gexp.SetFillColor(dijetISRColor)


    utiltools.store.append(gobs)
    utiltools.store.append(gexp)
    utiltools.store.append(gexist)

    l.Draw()
    l.SetBorderSize(0)
    l.SetFillStyle(0)

    canvastools.save(outfile)
