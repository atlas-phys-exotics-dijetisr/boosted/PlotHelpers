import ROOT

from prot import utiltools
from prot import plottools
from prot import canvastools
from prot import filetools

from hbbisrnote import constants

def main():
    colors=[]
    masses=[100,125,150,175,200,250,300]
    filelist=[]

    i=0
    for mass in masses:
        infile='hist-mc16_13TeV.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350P100_mRp{}_gSp25.root'.format(str(mass).replace('0',''))
        massstr='mR{}'.format(mass)
        filetools.filemap('OUT_fatjet_mc_test/{}'.format(infile),massstr)

        color=ROOT.gStyle.GetColorPalette(i)
        i+=1
        filelist.append(('{}:'.format(massstr),{'title':'{} GeV'.format(mass),'color':i}))

    plottools.plotsf(filelist,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77/Hcand_m',
                     hsopt='nostack',opt='hist',
                     scale=constants.lumi*1e3,
                     ytitle='Events',
                     legend=(0.55,0.65),legendncol=2,
                     text='SR - Z\' g_{q}=0.25',textpos=(0.6,0.75))
    canvastools.save('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77/Zprime/Hcand_m.pdf')
    plottools.plotsf(filelist,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85/Hcand_m',
                     hsopt='nostack',opt='hist',
                     scale=constants.lumi*1e3,
                     ytitle='Events',
                     legend=(0.55,0.65),legendncol=2,
                     text='CR - Z\' g_{q}=0.25',textpos=(0.6,0.75))
    canvastools.save('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85/Zprime/Hcand_m.pdf')    
