from prot import canvastools
from prot import filetools
from prot import plottools

from hbbisrnote import constants

def main():
    #
    # truth plots
    filetools.filemap('OUT_fatjet_mc_truth/hist-mc16_13TeV.345342.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb.truth.root','nofilt')
    filetools.filemap('OUT_fatjet_mc_truth/hist-mc16_13TeV.309450.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb_kt200.truth.root','kt200')

    plottools.plotsf([('nofilt:',{'title':'No Filter'}),
                      ('kt200:' ,{'title':'bornktmin=200'})],
                     'hbbisr_truth/Hcand_pt',
                     rebin=4,
                     logy=True,yrange=(5,1e4),ytitle='Events',
                     ratiorange=(0.5,1.1),
                     legend=(0.52,0.75),text='Powheg ggF H#rightarrowbb, Truth Level',lumi=80.5,textpos=(0.52,0.8))
    canvastools.save('hbbisr_truth/validggf/Hcand_pt.pdf')

    plottools.plotsf([('nofilt:',{'title':'No Filter'}),
                      ('kt200:' ,{'title':'bornktmin=200'})],
                     'hbbisr_truth/fatjet0_pt',
                     rebin=4,
                     logy=True,yrange=(5,1e4),ytitle='Events',
                     ratiorange=(0.5,1.1),
                     legend=(0.52,0.75),text='Powheg ggF H#rightarrowbb, Truth Level',lumi=80.5,textpos=(0.52,0.8))
    canvastools.save('hbbisr_truth/validggf/fatjet0_pt.pdf')

    plottools.plotsf([('nofilt:',{'title':'No Filter'}),
                      ('kt200:' ,{'title':'bornktmin=200'})],
                     'hbbisr_truth_fj480_hpt480/Hcand_m',
                     rebin=4,
                     xrange=(0,300),
                     logy=True,ytitle='Events',#yrange=(5,1e4),
                     ratiorange=(0.5,1.5),
                     legend=(0.52,0.75),text='Powheg ggF H#rightarrowbb, Truth Level',lumi=80.5,textpos=(0.52,0.8))
    canvastools.save('hbbisr_truth_fj480_hpt480/validggf/Hcand_m.pdf')

    #
    # full sim plots
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.345342.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb.fatjet.20180902-01_tree.root.root','sim-nofilt')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309450.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb_kt200.fatjet.20180902-01_tree.root.root','sim-kt200')

    plottools.plotsf([('sim-nofilt:',{'title':'No Filter'}),
                      ('sim-kt200:' ,{'title':'bornktmin=200'})],
                     '{}/Hcand_m'.format(constants.selectionSR.replace('hm40_','')),
                     xrange=(0,200),
                     ytitle='Events',#yrange=(5,1e4),
                     ratiorange=(0.5,1.5),
                     legend=(0.2,0.75),text='SR, Powheg ggF H#rightarrowbb',lumi=80.5,textpos=(0.2,0.8))
    canvastools.save('{}/validggf/Hcand_m.pdf'.format(constants.selectionSR.replace('hm40_','')))
