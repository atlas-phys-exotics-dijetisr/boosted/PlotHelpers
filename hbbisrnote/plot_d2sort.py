import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import style

import selectiontools

from hbbisrnote import constants

import copy
import shutil
import copy
import datetime
import os, os.path

from math import *

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'      ,'qcd')
    filetools.filemap('OUT_fatjet_mc/hist-v.root'        ,'v')
    filetools.filemap('OUT_fatjet_mc/hist-sig.root'      ,'sig')

    plottools.plotsf([('qcd:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'p_{T} ordering',
                                                                                                                                                   'fillcolor':None,
                                                                                                                                                   'color':ROOT.kBlack}),
                      ('qcd:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'D_{2} ordering',
                                                                                                                                                   'color':ROOT.kRed})],
                     'Hcand_m',
                     opt='hist',hsopt='nostack',
                     scale=constants.lumi*1e3,
                     xrange=(0,250),
                     ytitle='Events',
                     ratiorange=(0.5,1.5),
                     text='SR - Pythia QCD',textpos=(0.5,0.8),lumi=constants.lumi,
                     legend=(0.6,0.75))
    canvastools.save('d2sort/qcd/Hcand_m.pdf')
    

    plottools.plotsf([('v:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'p_{T} ordering',
                                                                                                                                                 'fillcolor':None,
                                                                                                                                                 'color':ROOT.kBlack}),
                      ('v:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'D_{2} ordering',
                                                                                                                                                 'color':ROOT.kRed})],
                     'Hcand_m',
                     opt='hist',hsopt='nostack',
                     scale=constants.lumi*1e3,
                     xrange=(0,250),
                     ytitle='Events',
                     ratiorange=(0.5,1.5),
                     text='SR - W/Z + jets',textpos=(0.5,0.8),lumi=constants.lumi,
                     legend=(0.6,0.75))
    canvastools.save('d2sort/v/Hcand_m.pdf')
    

    plottools.plotsf([('sig:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'p_{T} ordering',
                                                                                                                                                   'fillcolor':None,
                                                                                                                                                   'color':ROOT.kBlack}),
                      ('sig:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'D_{2} ordering',
                                                                                                                                                   'color':ROOT.kRed})],
                     'Hcand_m',
                     opt='hist',hsopt='nostack',
                     scale=constants.lumi*1e3,
                     xrange=(0,250),
                     ytitle='Events',
                     ratiorange=(0.5,1.5),
                     text='SR - Higgs',textpos=(0.2,0.8),lumi=constants.lumi,
                     legend=(0.6,0.75))
    canvastools.save('d2sort/higgs/Hcand_m.pdf')
    
    
