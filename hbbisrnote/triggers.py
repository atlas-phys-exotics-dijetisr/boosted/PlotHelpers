import ROOT
from prot import plottools
from prot import utiltools
from prot import filetools
from prot import canvastools
from prot import triggertools
from prot import textools

from math import *

import re

from hbbisrnote import constants

re_trigger=re.compile('.*(HLT_.*)')
re_hltpt=re.compile('.*_j([0-9]+).*')
re_hltm =re.compile('.*_([0-9]+)smcINF.*')
re_jetpt=re.compile('.*fjpt([0-9]+).*')

def roundpt(pt):
    return ceil(pt/10.)*10.

def trigger_plots(trigger,data,mc,reftrigger,lumi,varname='fatjet0_pt'):
    num=utiltools.Get('{}:/fatjet_{}_num/{}'.format(mc,trigger,varname))
    den=utiltools.Get('{}:/fatjet_{}_den/{}'.format(mc,trigger,varname))
    h_mc=num.Clone()
    h_mc.Divide(den)

    xrange=(300,500) if varname=='fatjet0_pt' else (0,100)
    re_hlttr=re_hltpt if varname=='fatjet0_pt' else re_hltm

    match=re_trigger.match(trigger)
    trigname=match.group(1)

    match=re_hlttr.match(trigger)
    hlttr=float(match.group(1))

    match=re_jetpt.match(trigger)
    jetpt=float(match.group(1)) if match!=None else None

    # Determine extra text in description
    text=[]
    text.append(trigname)
    text.append('ref {}'.format(reftrigger))
    if jetpt!=None:
        text.append('offline p_{{T}} > {}'.format(jetpt))
    
    h_data=triggertools.trigeff('{}:/fatjet_{}_num/{}'.format(data,trigger,varname),
                                '{}:/fatjet_{}_den/{}'.format(data,trigger,varname),
                                    fitrange=(hlttr,xrange[1]),text='{}'.format(trigger),textpos=(0.5,0.7),sim=False,lumi=lumi,
                                    xrange=xrange,yrange=(0,1))
    canvastools.save('trigger/triggerdata_{}_{}_{}.pdf'.format(data,trigger,varname))
    func=h_data.GetListOfFunctions()[0]
    effpt=func.GetX(0.99*func.GetParameter(2))
    print('Real 99',effpt)
    h_data.GetListOfFunctions().Clear()

    plottools.draw([(h_mc,{'title':'Pythia 8 QCD','opt':'hist','color':ROOT.kRed}),(h_data,{'title':'Data'})],
                   xrange=xrange,yrange=(0,1),
                   lumi=lumi,sim=False,
                   text='\n'.join(text),textpos=(0.45,0.25),legend=(0.2,0.9))
    canvastools.save('trigger/trigger_{}_{}_{}.pdf'.format(data,trigger,varname))

    return h_data,effpt
    

def main():
    filetools.filemap('OUT_fatjettrigger_mca/hist-qcd.root','mca')
    filetools.filemap('OUT_fatjettrigger_mcc/hist-qcd.root','mcc')
    filetools.filemap('OUT_fatjettrigger_data/hist-data15.root','data15')
    filetools.filemap('OUT_fatjettrigger_data/hist-data16.root','data16')
    filetools.filemap('OUT_fatjettrigger_data/hist-data17.root','data17')

    fh_trigeff=open('trigeff.tex','w')
    
    #
    # 2015 data

    # format for trigger list is trigger,reftrigger
    triggers=[]
    triggers.append(('HLT_j360','HLT_j260'))
    triggers.append(('HLT_j360_a10r_L1J100','HLT_j110'))              
    triggers.append(('HLT_j360_a10_lcw_sub_L1J100','HLT_j110'))

    # plot histogram for end
    theplots={}

    for trigger,reftrigger in triggers:
        theplots[trigger],pt=trigger_plots(trigger,'data15','mca',reftrigger=reftrigger,lumi=constants.lumi15)
        fh_trigeff.write('\\newcommand{\TrigEff%s}{%d}\n'%(textools.sanitizestring(trigger),roundpt(pt)))        
    
    plottools.draw([(theplots['HLT_j360']                   ,{'color':ROOT.kGray+2,'title':'HLT_j360'}),
                    (theplots['HLT_j360_a10_lcw_sub_L1J100'],{'color':ROOT.kBlue  ,'title':'HLT_j360_a10_lcw_sub_L1J100'}),
                    (theplots['HLT_j360_a10r_L1J100']       ,{'color':ROOT.kRed   ,'title':'HLT_j360_a10r_L1J100'})],
                    xrange=(300,500),yrange=(0,1),
                    legend=(0.5,0.4),
                    lumi=constants.lumi15,sim=False,
                    text='2015',textpos=(0.2,0.8))
    canvastools.save('trigger/trigger_data15_fatjet0_pt.pdf')

    #
    # 2016 data

    # format for trigger list is trigger,reftrigger
    triggers=[]
    triggers.append(('HLT_j380','HLT_j260'))
    triggers.append(('HLT_j420_a10r_L1J100','HLT_j260_a10r_L1J75'))
    triggers.append(('HLT_j420_a10_lcw_L1J100','HLT_j260_a10_lcw_L1J75'))

    # plot histogram for end
    theplots={}

    for trigger,reftrigger in triggers:
        theplots[trigger],pt=trigger_plots(trigger,'data16','mca',reftrigger=reftrigger,lumi=constants.lumi16)
        fh_trigeff.write('\\newcommand{\TrigEff%s}{%d}\n'%(textools.sanitizestring(trigger),roundpt(pt)))
    
    plottools.draw([(theplots['HLT_j380']               ,{'color':ROOT.kGray+2,'title':'HLT_j380'}),
                    (theplots['HLT_j420_a10_lcw_L1J100'],{'color':ROOT.kBlue  ,'title':'HLT_j420_a10_lcw_L1J100'}),
                    (theplots['HLT_j420_a10r_L1J100']   ,{'color':ROOT.kRed   ,'title':'HLT_j420_a10r_L1J100'})],
                    xrange=(300,500),yrange=(0,1),
                    legend=(0.55,0.4),
                    lumi=constants.lumi16,sim=False,
                    text='2016',textpos=(0.2,0.8))
    canvastools.save('trigger/trigger_data16_fatjet0_pt.pdf')

    #
    # 2017 data

    # format for trigger list is trigger,reftrigger
    triggers=[]
    triggers.append(('HLT_j420','HLT_j360',constants.lumi17))
    triggers.append(('HLT_j440_a10_lcw_subjes_L1J100','HLT_j260_a10_lcw_subjes_L1J100',40.6))
    triggers.append(('HLT_j460_a10_lcw_subjes_L1J100','HLT_j260_a10_lcw_subjes_L1J100',constants.lumi17))
    triggers.append(('HLT_j440_a10t_lcw_jes_L1J100','HLT_j260_a10t_lcw_jes_L1J100',40.8))
    triggers.append(('HLT_j460_a10t_lcw_jes_L1J100','HLT_j260_a10t_lcw_jes_L1J100',constants.lumi17))
    triggers.append(('HLT_j440_a10r_L1J100','HLT_j260_a10r_L1J100',40.6))
    triggers.append(('HLT_j460_a10r_L1J100','HLT_j260_a10r_L1J100',constants.lumi17))

    # plot histogram for end
    theplots={}

    for trigger,reftrigger,lumi in triggers:
        theplots[trigger],pt=trigger_plots(trigger,'data17','mcc',reftrigger=reftrigger,lumi=lumi)
        fh_trigeff.write('\\newcommand{\TrigEff%s}{%d}\n'%(textools.sanitizestring(trigger),roundpt(pt)))        
    
    plottools.draw([(theplots['HLT_j420']                      ,{'color':ROOT.kGray+2 ,'title':'HLT_j420'}),
                    (theplots['HLT_j440_a10_lcw_subjes_L1J100'],{'color':ROOT.kBlue   ,'title':'HLT_j440_a10_lcw_subjes_L1J100','markerstyle':ROOT.kOpenCircle}),
                    (theplots['HLT_j460_a10_lcw_subjes_L1J100'],{'color':ROOT.kBlue   ,'title':'HLT_j460_a10_lcw_subjes_L1J100'}),
                    (theplots['HLT_j440_a10r_L1J100']          ,{'color':ROOT.kRed    ,'title':'HLT_j440_a10r_L1J100'          ,'markerstyle':ROOT.kOpenCircle}),
                    (theplots['HLT_j460_a10r_L1J100']          ,{'color':ROOT.kRed    ,'title':'HLT_j460_a10r_L1J100'}),
                    (theplots['HLT_j440_a10t_lcw_jes_L1J100']  ,{'color':ROOT.kMagenta,'title':'HLT_j440_a10r_L1J100'          ,'markerstyle':ROOT.kOpenCircle}),
                    (theplots['HLT_j460_a10t_lcw_jes_L1J100']  ,{'color':ROOT.kMagenta,'title':'HLT_j460_a10r_L1J100'})],
                    xrange=(300,500),yrange=(0,1),
                    legend=(0.2,0.7),
                    lumi=constants.lumi17,sim=False,
                    text='2017',textpos=(0.2,0.8))
    canvastools.save('trigger/trigger_data17_fatjet0_pt.pdf')

    #
    # 2017 data - substructure

    # format for trigger list is trigger,reftrigger
    triggers=[]
    triggers.append(('fjpt420_HLT_j390_a10t_lcw_jes_30smcINF_L1J100','HLT_j260_a10t_lcw_jes_L1J75',40.6))
    triggers.append(('fjm50_HLT_j390_a10t_lcw_jes_30smcINF_L1J100','HLT_j260_a10t_lcw_jes_L1J75',40.6))    
    triggers.append(('fjpt440_HLT_j420_a10t_lcw_jes_40smcINF_L1J100','HLT_j260_a10t_lcw_jes_L1J75',constants.lumi17))
    triggers.append(('fjm70_HLT_j420_a10t_lcw_jes_40smcINF_L1J100','HLT_j260_a10t_lcw_jes_L1J75',constants.lumi17))

    # plot histogram for end
    theplots={}

    for trigger,reftrigger,lumi in triggers:
        theplots[trigger],pt=trigger_plots(trigger,'data17','mcc',reftrigger=reftrigger,lumi=lumi)
        theplots[trigger],m=trigger_plots(trigger,'data17','mcc',reftrigger=reftrigger,lumi=lumi,varname='fatjet0_m')

        if trigger.startswith('fjm'):  fh_trigeff.write('\\newcommand{\TrigEff%sPt}{%d}\n'%(textools.sanitizestring(trigger[7:]),roundpt(pt)))
        if trigger.startswith('fjpt'): fh_trigeff.write('\\newcommand{\TrigEff%sM}{%d}\n' %(textools.sanitizestring(trigger[8:]),roundpt(m)))

    plottools.draw([(theplots['fjpt420_HLT_j390_a10t_lcw_jes_30smcINF_L1J100']                      ,{'color':ROOT.kBlue,'title':'HLT_j390_a10t_lcw_jes_30smcINF_L1J100 (p_{T,J} > 420 GeV)'}),
                    (theplots['fjpt440_HLT_j420_a10t_lcw_jes_40smcINF_L1J100']                      ,{'color':ROOT.kRed ,'title':'HLT_j420_a10t_lcw_jes_40smcINF_L1J100 (p_{T,J} > 440 GeV)'})],
                   xrange=(0,100),yrange=(0,1),
                   legend=(0.2,0.7),
                   lumi=constants.lumi17,sim=False,
                   text='2017',textpos=(0.2,0.8))
    canvastools.save('trigger/trigger_data17_substr_fatjet0_m.pdf')
