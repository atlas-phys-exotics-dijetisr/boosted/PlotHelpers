from prot import canvastools
from prot import filetools
from prot import plottools
from prot import utiltools
from prot import textools

import ROOT

from hbbisrnote import constants

def plot_flavoursplit(mR):
    plottools.plotsf([('mR{}_qqj:/'.format(mR),{'title':'light quarks',
                                                'fillcolor':ROOT.kBlue,
                                                'opt':'hist'}),
                      ('mR{}_bbj:/'.format(mR),{'title':'b quarks',
                                                'fillcolor':ROOT.kRed,
                                                'opt':'hist'}),                                                
                      ('mR{}_jjj:/'.format(mR),{'title':'inclusive',
                                                'color':ROOT.kBlack,
                                                'opt':'e'}),
                        ],
                         'hbbisr_truth_fj480/fatjet0_m',
                         hsopt='nostack',stackgroup=[(0,1),2],
                         ytitle='Events',
                         xrange=(0,250),
                         scale=constants.lumi*1e3,
                         ratio=1,ratiorange=(0.8,1.2),ratiotitle='split/inclusive',
                         legend=(0.2,0.7),legendncol=1,
                         text='m_{{R}}={} GeV, g_{{SM}}=0.25'.format(mR),textpos=(0.2,0.8),lumi=constants.lumi)
    canvastools.save('samplevalidation/zprime/flavoursplit_mR{}_fatjet0_pt.pdf'.format(mR))
    
def main():
    #
    # File map
    filetools.filemap('OUT_fatjet_truth/hist-MC15.999999.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350P100_mRp125_gSp25.TRUTH1.root','mR125_jjj')
    filetools.filemap('OUT_fatjet_truth/hist-MC15.999999.MGPy8EG_N30LO_A14N23LO_dmA_qqj_Jet350P100_mRp125_gSp25.TRUTH1.root','mR125_qqj')
    filetools.filemap('OUT_fatjet_truth/hist-MC15.999999.MGPy8EG_N30LO_A14N23LO_dmA_bbj_Jet350P100_mRp125_gSp25.TRUTH1.root','mR125_bbj')

    filetools.filemap('OUT_fatjet_truth/hist-MC15.999999.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350P100_mRp2_gSp25.TRUTH1.root','mR200_jjj')
    filetools.filemap('OUT_fatjet_truth/hist-MC15.999999.MGPy8EG_N30LO_A14N23LO_dmA_qqj_Jet350P100_mRp2_gSp25.TRUTH1.root','mR200_qqj')
    filetools.filemap('OUT_fatjet_truth/hist-MC15.999999.MGPy8EG_N30LO_A14N23LO_dmA_bbj_Jet350P100_mRp2_gSp25.TRUTH1.root','mR200_bbj')

    #
    # Plot
    plot_flavoursplit(mR=125)
    plot_flavoursplit(mR=200)
