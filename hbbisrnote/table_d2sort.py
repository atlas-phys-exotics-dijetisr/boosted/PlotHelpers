from prot import filetools
from prot import utiltools
from prot import textools

import ROOT

from math import *

from hbbisrnote import constants

def calc_soversqrtb(selection,signal,massWindow,fh_tex):
    h_s=utiltools.Get('{}:/{}/Hcand_m'.format(signal,selection))
    h_b=utiltools.Get('bkg:/{}/Hcand_m'.format(selection))

    s=h_s.Integral(h_s.FindBin(massWindow[0]),h_s.FindBin(massWindow[1]))*constants.lumi*1e3
    b=h_b.Integral(h_b.FindBin(massWindow[0]),h_b.FindBin(massWindow[1]))*constants.lumi*1e3

    sgn=s/sqrt(b)

    print(signal,selection,sgn)
    textools.write_command(fh_tex,'D2SortSoverSqrtB{}{}'.format(signal,selection),sgn,fmt=':0.2f')

    return sgn

def main():
    filetools.filemap('OUT_fatjet_mc/hist-sig.root','sig')
    filetools.filemap('OUT_fatjet_mc/hist-v.root','v')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    fh_tex=open('d2sortsoversqrtb.tex','w')

    hd2480=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'sig', (100,150), fh_tex=fh_tex)
    hd2250=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt250_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'sig', (100,150), fh_tex=fh_tex)
    hpt480=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'sig', (100,150), fh_tex=fh_tex)
    hpt250=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt250_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'sig', (100,150), fh_tex=fh_tex)

    vd2480=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'v', (65,115), fh_tex=fh_tex)
    vd2250=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt250_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'v', (65,115), fh_tex=fh_tex)
    vpt480=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'v', (65,115), fh_tex=fh_tex)
    vpt250=calc_soversqrtb('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt250_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', 'v', (65,115), fh_tex=fh_tex)

    textools.write_command(fh_tex,'D2SortSoverSqrtBsigPtImprovement',(hd2250/hd2480-1)*100,fmt=':0.0f')
    textools.write_command(fh_tex,'D2SortSoverSqrtBsigD2Improvement',(hd2250/hpt250-1)*100,fmt=':0.0f')

    textools.write_command(fh_tex,'D2SortSoverSqrtBvD2Improvement480',(vd2480/vpt480-1)*100,fmt=':0.0f')
    textools.write_command(fh_tex,'D2SortSoverSqrtBvD2Improvement250',(vd2250/vpt250-1)*100,fmt=':0.0f')
    
    fh_tex.close()
