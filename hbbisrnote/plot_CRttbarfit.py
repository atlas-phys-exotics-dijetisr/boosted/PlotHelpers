from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools

import fitdatamctools

from hbbisrnote import constants

import ROOT

def main():
    #
    # Create histograms
    fh_data=filetools.open('OUT_fatjet_data/hist-data.root')
    fh_fit =filetools.open('batchUnige/Step2_setLimitsOneMassPoint_ZPrime0p10_mZ125_doSwift.root')

    h_ttbar_postfit=fh_fit .Get('BestSignalPrediction')
    h_ttbar_prefit =fh_fit .Get('h_sr')
    h_singletop    =fh_fit .Get('signal_2')
    h_wlnu         =fh_fit .Get('signal_3')
    h_data_obins   =fh_data.Get('{}/Hcand_m'.format(constants.selectionTTbarCR))

    print((h_ttbar_prefit.GetNbinsX(), h_ttbar_prefit.GetBinLowEdge(1), h_ttbar_prefit.GetBinLowEdge(h_ttbar_prefit.GetNbinsX()+2)))
    h_data=ROOT.TH1F('data','',h_ttbar_prefit.GetNbinsX(), h_ttbar_prefit.GetBinLowEdge(1), h_ttbar_prefit.GetBinLowEdge(h_ttbar_prefit.GetNbinsX()+1))
    for binidx in range(h_data.GetNbinsX()+2):
        x=h_data.GetBinCenter(binidx)
        h_data.SetBinContent(binidx, h_data_obins.GetBinContent(h_data_obins.FindBin(x)))
        h_data.SetBinError  (binidx, h_data_obins.GetBinError  (h_data_obins.FindBin(x)))

    # output
    #fh_fitTTbarCR=open('fitTTbarCR.tex','w')
    
    #
    # The prefit plot
    plottools.plots([(h_wlnu,{'title':'W#rightarrow l#nu',
                              'fillcolor':ROOT.kMagenta,
                              'opt':'hist'}),                                                  
                     (h_singletop,{'title':'Wt',
                                   'fillcolor':ROOT.kGreen+4,
                                   'opt':'hist'}),                     
                     (h_ttbar_prefit,{'title':'t#bar{t}',
                                      'fillcolor':ROOT.kGreen+2,
                                      'opt':'hist'}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2),3),
                    text='CRttbar - prefit',lumi=constants.lumi,sim=False,textpos=(0.2,0.8),
                    legend=(0.6,0.93),legendncol=2,
                    logy=False,yrange=(0,120),ytitle='Events',
                    xrange=(70,230),
                    ratio=0,ratiorange=(0.,2.),ratiotitle='Data/MC')
    canvastools.save('{}/fitTTbarCR/prefit_Hcand_m.pdf'.format(constants.selectionTTbarCR))

    #
    # The prefit plot
    plottools.plots([(h_wlnu,{'title':'W#rightarrow l#nu',
                              'fillcolor':ROOT.kMagenta,
                              'opt':'hist'}),                                                  
                     (h_singletop,{'title':'Wt',
                                   'fillcolor':ROOT.kGreen+4,
                                   'opt':'hist'}),                     
                     (h_ttbar_postfit,{'title':'t#bar{t}',
                                       'fillcolor':ROOT.kGreen+2,
                                       'opt':'hist'}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2),3),
                    text='CRttbar - postfit',lumi=constants.lumi,sim=False,textpos=(0.2,0.8),
                    legend=(0.6,0.93),legendncol=2,
                    logy=False,yrange=(0,120),ytitle='Events',
                    xrange=(70,230),
                    ratio=0,ratiorange=(0.,2.),ratiotitle='Data/MC')
    canvastools.save('{}/fitTTbarCR/postfit_Hcand_m.pdf'.format(constants.selectionTTbarCR))
    
    #
    # Fit with everything float
    # result=fitdatamctools.fitDataMC([h_singletop,h_ttbar,h_wlnu],h_data,fix=[0,2])
    # plottools.plots([(h_wlnu,{'title':'W#rightarrow l#nu',
    #                               'fillcolor':ROOT.kMagenta,
    #                               'opt':'hist'}),
    #                               (h_singletop,{'title':'Wt',
    #                                'fillcolor':ROOT.kGreen+4,
    #                                'opt':'hist'}),
    #                  (h_ttbar,{'title':'t#bar{{t}} (#mu={0:0.2f} #pm {1:0.2f})'.format(result.Parameter(1),result.ParError(1)),
    #                               'fillcolor':ROOT.kGreen+2,
    #                               'opt':'hist',
    #                               'scale':result.Parameter(1)}),
    #                  (h_data,{'title':'Data'})],
    #                 hsopt='nostack',stackgroup=((0,1,2),3),
    #                 text='CRttbar - Float All',lumi=constants.lumi,sim=False,textpos=(0.2,0.8),
    #                 legend=(0.5,0.93),legendncol=2,
    #                 logy=False,yrange=(0,120),ytitle='Events',
    #                 xrange=(60,300),
    #                 ratio=0,ratiorange=(0.,2.),ratiotitle='Data/MC')
    # canvastools.save('{}/fitTTbarCR/floatall_Hcand_m.pdf'.format(constants.selectionTTbarCR))

    # textools.write_command(fh_fitTTbarCR,'FitTTbarCRFloatAllMuTTbar',result.Parameter(2),fmt=':0.01f')
    # textools.write_command(fh_fitTTbarCR,'FitTTbarCRFloatAllMuTTbarErr',result.ParError(2),fmt=':0.01f')
    
    # #
    # #
    # fh_fitTTbarCR.close()
