from prot import plottools
from prot import filetools
from prot import canvastools
from prot import utiltools

import ROOT

def calc_unc(ptcut):
    binmin=utiltools.Get('ggf:/PDF4LHC15_nlo_30_pdfas_000/Zprime_pt').FindBin(ptcut)
    binmax=utiltools.Get('ggf:/PDF4LHC15_nlo_30_pdfas_000/Zprime_pt').GetNbinsX()
    print(ptcut,utiltools.Get('ggf:/PDF4LHC15_nlo_30_pdfas_000/Zprime_pt').GetBinLowEdge(binmin))

    xs_nom=utiltools.Get('ggf:/PDF4LHC15_nlo_30_pdfas_000/Zprime_pt').Integral(binmin,binmax)/0.57

    print('{:0.1f}'.format(xs_nom*1e3))
    

def main():
    filetools.filemap('test.root','ggf')

    calc_unc(400)
    calc_unc(430)
    calc_unc(450)
    calc_unc(500)
