from prot import plottools
from prot import filetools
from prot import canvastools
from prot import utiltools

import ROOT

def main():
    filetools.filemap('ggZqqH.root','sig')
    title='ggZH, Z#rightarrowqq'

    hxsec=utiltools.Get('sig:/nominal/Zprime_pt')
    hraw =utiltools.Get('sig:/unweighted/Zprime_pt')
    heff=hxsec.Clone()
    for i in range(0,hxsec.GetNbinsX()+2):
        # Update eff entries
        mu=hxsec.GetBinContent(i)
        st=hxsec.GetBinError  (i)
        eff=(mu*mu)/(st*st) if st>0 else 0.
        heff.SetBinContent(i, eff)

    plottools.plots([(hxsec,{'title':'Expected Yield','scale':36.2e3,'fillcolor':None}),
                     (hraw,{'title':'Raw Events','color':ROOT.kBlue}),
                     (heff,{'title':'Effective Stats','color':ROOT.kRed})],
                    opt='hist',hsopt='nostack',
                    ratio=0,ratiorange=(0,100),ratiotitle='1/Expected',
                    xtitle='p_{T,H} [GeV]',xrange=(0,1000),
                    ytitle='Events',logy=True,#yrange=(1e-1,1e5),
                    text='{}, H#rightarrowbb\nN_{{gen}}={:0.2g}'.format(title,hraw.GetEntries()),textpos=(0.45,0.75),legend=(0.65,0.75),lumi=36.2)
    canvastools.save('sysvalid/vh/neff.pdf')
    
