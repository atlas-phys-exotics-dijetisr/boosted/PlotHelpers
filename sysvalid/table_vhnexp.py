from prot import plottools
from prot import filetools
from prot import canvastools
from prot import utiltools

import ROOT

def calc_unc(ptcut):
    binmin=utiltools.Get('sig:/nominal/Zprime_pt').FindBin(ptcut)
    binmax=utiltools.Get('sig:/nominal/Zprime_pt').GetNbinsX()
    print(ptcut,utiltools.Get('sig:/nominal/Zprime_pt').GetBinLowEdge(binmin))

    xs_nom=utiltools.Get('sig:/nominal/Zprime_pt').Integral(binmin,binmax)

    print('{:0.1f}'.format(xs_nom*36.2e3*5.824E-01))
    

def main():
    filetools.filemap('WpH_filt.root','sig')
    calc_unc(0)    
    calc_unc(500)

    filetools.filemap('WmH_filt.root','sig')
    calc_unc(0)    
    calc_unc(500)

    filetools.filemap('ZH_filt.root','sig')
    calc_unc(0)    
    calc_unc(500)
