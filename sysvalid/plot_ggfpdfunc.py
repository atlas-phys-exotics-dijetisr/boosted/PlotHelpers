from prot import plottools
from prot import filetools
from prot import canvastools

import ROOT

def main():
    filetools.filemap('test-kt200.root','ggf')

    hs_pdfs=[('ggf:/nominal',{'title':'NNPDF30_nnlo_as_0118'})]
    for i in range(0,101):
        hs_pdfs.append(('ggf:/NNPDF30_nlo_as_0118_{:03d}'.format(i),{'title':'NNPDF30_nlo_as_0118' if i==0 else '','color':ROOT.Blind1}))

    for i in range(0,33):
        hs_pdfs.append(('ggf:/PDF4LHC15_nlo_30_pdfas_{:03d}'.format(i),{'title':'PDF4LHC15_nlo_30_pdfas' if i==0 else '','color':ROOT.Blind2}))

    hs_pdfs.append(('ggf:/MMHT2014nlo68clas118',{'title':'MMHT2014nlo68clas118','color':ROOT.Blind3}))
    hs_pdfs.append(('ggf:/CT14nlo',{'title':'CT14nlo','color':ROOT.Blind4}))
        
    plottools.plotsf(hs_pdfs,
                     'fatjet0_m',
                     hsopt='nostack',opt='hist',
                     xtitle='leading jet mass [GeV]',xrange=(0,200),
                     ytitle='#sigma [pb]',
                     ratio=0,ratiorange=(0.7,1.3),
                     legend=(0.2,0.73),text='ggF (filtered), p_{T,H}>500 GeV',textpos=(0.2,0.8))
    canvastools.save('sysvalid/ggf/pdf.pdf')

    plottools.plotsf(hs_pdfs,
                     'fatjet0_m',
                     hsopt='nostack',opt='hist',
                     normalize=(110,130),
                     xtitle='leading jet mass [GeV]',xrange=(0,200),
                     ytitle='#sigma [pb]',
                     ratio=0,ratiorange=(0.9,1.1),
                     legend=(0.2,0.73),text='ggF (filtered), p_{T,H}>500 GeV',textpos=(0.2,0.8))
    canvastools.save('sysvalid/ggf/pdf-norm.pdf')
    
