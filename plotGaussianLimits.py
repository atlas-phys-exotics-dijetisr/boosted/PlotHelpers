from prot import filetools
from prot import plottools
from prot import canvastools

import ROOT

import selectiontools

def plotGaussianLimits(*d_sels):
    lumi=3200. #3234.64
    graphs=[]

    for d_sel in d_sels:
        massesUsed=d_sel[0].Get('massesUsed')
        if massesUsed==None: continue

        CLsPerMass_widthToMass=None #=d_sel[0].Get('CLsPerMass_widthToMass100')
        width=0
        for key in d_sel[0].GetListOfKeys():
            if not key.GetName().startswith('CLsPerMass_widthToMass'): continue
            CLsPerMass_widthToMass=key.ReadObj()
            width=float(key.GetName()[22:])/1000
            break

        g=ROOT.TGraph()
        g.SetTitle('#sigma_{R}/m_{R} = %0.2f'%width)
        graphs.append((g,d_sel[1]))
        for gidx in range(len(massesUsed)):
            mass=massesUsed[gidx]
            CLs=CLsPerMass_widthToMass[gidx]/lumi
            g.SetPoint(gidx,mass,CLs)

    plottools.graphs(graphs,opt='pl',logy=True,xtitle='m_{R} [GeV]',ytitle='#sigma #times A #times BR [pb]',text={'title':d_sel[0].GetName(),'lumi':lumi/1e3},textpos=(0.2,0.25),legend=(0.6,0.9),yrange=(1e-2,1),xrange=(570,1200))
    canvastools.save('gaussianLimit.pdf')

def main(infiles):
    canvastools.savepath+='/plots'
    selectiontools.loop(plotGaussianLimits,*infiles)

