#!/usr/bin/env python

import ROOT

import sys
import glob
import os, os.path
import fnmatch
import argparse

def recursive_rescale(d,scale,exclude=[]):
    keys=d.GetListOfKeys()

    for key in list(keys):
        name=key.GetName()
        obj=key.ReadObj()
        if obj.InheritsFrom(ROOT.TH1.Class()):
            d.cd()

            # Check for exclude
            path=d.GetPath()+'/'+obj.GetName()
            skip=False
            for ex in exclude:
                if fnmatch.fnmatch(path,ex): skip=True
            if skip: continue

            # Scale
            print(path)
            obj.Scale(scale)
            obj.Write("", ROOT.TObject.kOverwrite)
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            recursive_rescale(obj,scale,exclude=exclude)
        obj.Delete()

parser = argparse.ArgumentParser(description="Rescale all histograms in a TFile by some factor")
parser.add_argument('input',help="TFile with histograms that need to be rescaled.")

parser.add_argument('scale',type=float,help="Scale to apply.")
parser.add_argument('--exclude','-e',action='append',default=[],help="Exclude glob pattern.")
args = parser.parse_args()

fh=ROOT.TFile.Open(args.input,'update')
recursive_rescale(fh,args.scale,exclude=args.exclude)
