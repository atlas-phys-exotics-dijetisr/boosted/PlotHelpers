import ROOT
import re

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import textools
from prot import canvastools
from prot import style

import selectiontools

import pprint
from math import *

def acceff(d_file,**kwargs):
    print(canvastools.savepath)
    aename=kwargs.get('aename','default')
    fh=canvastools.fopen('acceff_%s.tex'%aename,'w')
    fhpy=canvastools.fopen('acceff_%s.py'%aename,'w')

    summarydict={}
    
    t=d_file[0].Get('tree')
    for p in t:
        pname='%smR%dmDM%dgSM%0.2fgDM%0.2f'%(aename,p.mR,p.mDM,p.gSM,p.gDM)
        pname=textools.sanitizestring(pname)

        fh.write('\\newcommand{\AccEff%s}{%0.2f}\n'%(pname,p.seleff))
        fh.write('\\newcommand{\Acc%s}{%0.2f}\n'%(pname,p.acc))
        fh.write('\\newcommand{\Eff%s}{%0.2f}\n'%(pname,p.eff))

        # Update the summary dictionary
        if p.gSM not in summarydict:
            summarydict[p.gSM]={}
        summarydict[p.gSM][p.mR]={'acc':p.acc,
                                  'eff':p.eff,
                                  'acceff':p.seleff}

    fhpy.write(pprint.pformat(summarydict))

    fh.close()
    fhpy.close()

def main(file,**kwargs):
    canvastools.savesetup('acceff',**kwargs)
    selectiontools.loop(acceff,file,**kwargs)
    canvastools.savereset()
