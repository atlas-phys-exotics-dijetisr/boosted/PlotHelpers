import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import systools
from prot import style

import selectiontools

import shutil
import copy
import datetime
import os, os.path

def compare_systematics(d,**kwargs):
    nameTag=kwargs.get('nameTag','none')
    mjjrange=kwargs.get('mjjrange',None)

    extra={}
    
    canvastools.savepath+='/syscomp'
    for key in d[0].GetListOfKeys():
        name=key.GetName()
        obj=key.ReadObj()

        if name!='Zprime_mjj_var': continue
        if not obj.InheritsFrom(ROOT.TH1F.Class()): continue

        if name=='Zprime_mjj' or name=='Zprime_mjj_var':
            minBin=max(obj.FindBin(mjjrange[0]),1)
            maxBin=obj.FindBin(mjjrange[1])
            xrange=(obj.GetBinLowEdge(minBin),obj.GetBinLowEdge(maxBin))
            extra['xrange']=xrange
            extra['logy']=False

        systs=systools.get_syslist(obj,True)
        for sysname,syslist in systs.items():
            syshlist=[]
            if len(syslist)==0: continue
            elif len(syslist)==1:
                syshlist=[(syslist[0],{'title':'Variation','color':ROOT.kBlue})]
            elif len(syslist)==2:
                syshlist=[(syslist[0],{'title':'Up'       ,'color':ROOT.kBlue}),
                          (syslist[1],{'title':'Down'     ,'color':ROOT.kRed})
                          ]
            else:
                continue

            plottools.plots([(obj,{'title':'Nominal','fillcolor':0})]+syshlist,hsopt='nostack',opt='hist',ratio=0,ratiorange=(0.6,1.4),style=name,text=sysname,textpos=(0.2,0.25),ytitle='#sigma [pb]',legend=(0.7,0.8),**extra)
            canvastools.save('%s-%s-%s.pdf'%(nameTag,name,sysname))

def main(file,**kwargs):
    #
    selectiontools.loop(compare_systematics,file,**kwargs)

