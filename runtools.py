import csv

class RunList:
    def __init__(self,path):
        self.runs={}
        self.totallumi=0

        #
        # Find all runs
        with open(path) as fhlumi:
            reader = csv.DictReader(fhlumi)
            for row in reader:
                lumi=float(row['LAr Corrected'])/1000.
                self.runs[int(row['Run'])]=lumi
                self.totallumi+=lumi

    def lumisteps(self):
        steps=[]
        totallumi=0
        lumisincesave=0
        for run in sorted(self.runs.keys()):
            lumi=self.runs[run]
            lumisincesave+=lumi
            totallumi+=lumi
            if lumisincesave>0.1:
                steps.append(totallumi)
                lumisincesave=0.
        if lumisincesave>0.1:
            steps.append(totallumi)
            lumisincesave=0.
        return steps
        
